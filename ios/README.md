# ExposeBox

[![CI Status](https://img.shields.io/travis/Evgeniy Shavluga/ExposeBoxTest.svg?style=flat)](https://travis-ci.org/Evgeniy Shavluga/ExposeBoxTest)
[![Version](https://img.shields.io/cocoapods/v/ExposeBoxTest.svg?style=flat)](https://cocoapods.org/pods/ExposeBoxTest)
[![License](https://img.shields.io/cocoapods/l/ExposeBoxTest.svg?style=flat)](https://cocoapods.org/pods/ExposeBoxTest)
[![Platform](https://img.shields.io/cocoapods/p/ExposeBoxTest.svg?style=flat)](https://cocoapods.org/pods/ExposeBoxTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Installation

ExposeBox is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ExposeBox'
```

## Usage

Import the framework (required) 
```swift
import ExposeBox
```
Configure with  your *companyId* (required),  your *appId* (optional) , COOPPA, GDPR
All data sent before setting companyId will be ignored. 
The APP won't send the data if the GDPR or COOPPA values are set to true.
```swift
ExposeBox.shared.configure(companyId: "COMPANY_ID", appId: "APP_ID", coppaCompliance: false, GDPR: false)
```
*companyId* is your company's ID, *appId* is your app's ID, both provided by ExposeBox.

Then you can use ExposeBox across your app. 

## Methods

### Track a page view
```swift
ExposeBox.shared.view(name: "MainView")
```

### Set Categories
```swift
ExposeBox.shared.setCategories(["Category1", "Category2"]) 
```
### Set Products
```swift
ExposeBox.shared.setProducts(["ID1", "ID2"])
```
In case of a single product, you can provide an array with a single element

### Set Tags
```swift
ExposeBox.shared.setTags(["Key" : ["Value1", "Value2"]])
```

### Add and Remove from Cart
Use this when your customer is adding or removing items from the cart, where *productId*  is your product's SKU, quantity  is the quantity of the same product added, and price  the product's unit price
```swift
ExposeBox.shared.addToCart(productId: "ID", quantity: Int, unitPrice: Double)

ExposeBox.shared.removeFromCart(productId: "ID", quantity: Int)
```
### Add and Remove from Wishlist
```swift
ExposeBox.shared.addToWishlist(productId: "ID")

ExposeBox.shared.removeFromWishlist(productId: "ID")
```

### Customer Data
Setting customer details enables ExposeBox send automated emails and synchronise offline data with online data.

Use this when the customer is logged in and the data is available.

First, create the *CustomerData* object
```swift
let customerData = ExposeBoxCustomerData(email: "EMAIL", customerId: "CustomerId")
customerData.firstName = "FIRST"
customerData.lastName = "LAST"
customerData.address1 = "address1"
customerData.address2 = "address2"
customerData.city = "city"
customerData.country = "country"
customerData.state = "state"
customerData.zipCode = "zipCode"
customerData.freeData = [String: String]
ExposeBox.shared.setCustomerData(customerData)
```
Other fields like city, zip code, address, phone, etc are available via additional info.

Then send it through ExposeBox

### Custom Event
You can send custom events with data
```swift
ExposeBox.shared.customEvent(name: "EVENT", data: [String : Any])
```
Where *eventName* is the event name that will appear on your dashboard, and data is a Dictionary with your custom event data.

### Click
```swift
ExposeBox.shared.click(placementId: "ID", widgetId: Int, productId: "ID", additionalData: [String: String]?)
```
### Real Impressions
```swift
let batches = [RealImpressionBatch(placementId: "ID", widgetId: Int, items: [String : Any])]

ExposeBox.shared.realImpression(batches)
```
### Order (Conversions)
Notifies ExposeBox that a conversion was made. This should be sent when a conversion event has occurred (e.g. a “thank you” page after checkout)

First, create the cart products that were in the order, then send it through ExposeBox with *orderId* and *totalPrice* (optional) (*orderId* should be your order ID, which you usually get after an order was placed).
```swift
let products = [ExposeBoxCartProduct(productId: "ID", quantity: Int, unitPrice: Double)]

ExposeBox.shared.conversion(orderId: "ID", totalPrice: Double, cartProducts: products)
```
### Get Recommendations
After you set up your placements in the ExposeBox dashboard, you can get product recommendations for those placements. Provide placement IDs in a string array, like in the example below for placements ID1 and ID2. The response will contain a List of ExposeBox Placements
```swift
ExposeBox.shared.recommendations(placementIds: ["ID1", "ID2"]) { objects, error in
  // YOUR HANDLERS
}
```

### Reset Session
Use this to reset session start time to current
```swift
ExposeBox.shared.resetSession()
```

## Author

ExposeBox (C)

## License

ExposeBox is available under the MIT license. See the LICENSE file for more info.
