import XCTest
import ExposeBox

class Tests: XCTestCase {

    var isCoppaComplianceEnable = true
    var isGDPREnable = true

    enum Constants {
        static let companyId = "30510"
        static let productId =  "product-8"
        static let productId2 =  "product-7"
        static let widgetId = 2180
    }
    
    override func setUp() {
        super.setUp()
        ExposeBox.shared.configure(companyId: Constants.companyId, appId: "test.exposebox.com", coppaCompliance: isCoppaComplianceEnable, GDPR: isGDPREnable)
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testCoppaComplianceEnable() {
       XCTAssertEqual(ExposeBox.shared.hasCoppaCompliance, isCoppaComplianceEnable)
    }

    func testGDPREnable() {
        XCTAssertEqual(ExposeBox.shared.hasGDPR, isGDPREnable)
    }

    func testView()  {
        var success: Bool = false
        let expectation = self.expectation(description: "testView")
        ExposeBox.shared.view(viewName: "mainView", onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }
    
    func testClick() {
        var success: Bool = false
        let expectation = self.expectation(description: "testClick")
        ExposeBox.shared.click(placementId: "mainPagePlacement", widgetId: Constants.widgetId, productId: Constants.productId, additionalData: ["bsrId":"sdhjd83f"], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testProduct() {
        var success: Bool = false
        let expectation = self.expectation(description: "testProduct")
        ExposeBox.shared.setProducts(productIds: [Constants.productId, Constants.productId2], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testSetCategories() {
        var success: Bool = false
        let expectation = self.expectation(description: "testSetCategories")
        ExposeBox.shared.setCategories(categoriesNames: ["testCategory", "testCategory2"], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testCustomEvent() {
        var success: Bool = false
        let expectation = self.expectation(description: "testCustomEvent")
        ExposeBox.shared.customEvent(name: "TestCustomEvent", data: ["Test": "Test"], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testAddToWishlist() {
        var success: Bool = false
        let expectation = self.expectation(description: "testAddToWishlist")
        ExposeBox.shared.addToWishlist(productId: Constants.productId, onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testRemoveFromWishlist() {
        var success: Bool = false
        let expectation = self.expectation(description: "testRemoveFromWishlist")
        ExposeBox.shared.removeFromWishlist(productId: Constants.productId, onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testAddToCart() {
        var success: Bool = false
        let expectation = self.expectation(description: "testAddToCart")
        ExposeBox.shared.addToCart(productId: Constants.productId, quantity: 5, unitPrice: 40.0, onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testRemoveFromCart() {
        var success: Bool = false
        let expectation = self.expectation(description: "testRemoveFromCart")
        ExposeBox.shared.removeFromCart(productId: Constants.productId, quantity: 1, onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testConversion() {
        var success: Bool = false
        let expectation = self.expectation(description: "testConversion")
        let product1 = ExposeBoxCartProduct(productId: Constants.productId, quantity: 2, unitPrice: 200.02)
        let product2 = ExposeBoxCartProduct(productId: Constants.productId2, quantity: 3, unitPrice: 320.30)
        ExposeBox.shared.conversion(orderId: "3377777", totalPrice: 600.45, cartProducts: [product1, product2], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testSetTags() {
        var success: Bool = false
        let expectation = self.expectation(description: "testSetTags")
        ExposeBox.shared.setTags(tags: ["test": ["tagTest"]], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testSetCustomerData() {
        var success: Bool = false
        let expectation = self.expectation(description: "testSetCustomerData")
        let customerData = ExposeBoxCustomerData(email: "test@gmail.com", customerId: "2014")
        ExposeBox.shared.setCustomerData(customerData: customerData, onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testRecommendations() {
        var success: Bool = false
        let expectation = self.expectation(description: "testRecommendations")
        ExposeBox.shared.recommendations(placementIds: ["mainPagePlacement"], onSuccess: { response in
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }

    func testRealImpression() {
        var success: Bool = false
        let expectation = self.expectation(description: "testRealImpression")
        let item = ExposeBoxRealImpressionBatch(placementId: "mainPagePlacement", widgetId: 2180, items: [["id": "gtest12", "itemType": "product",
                                                                                                           "catalogId": "default", "category": "leonardo-negev-beer-sheba", "brand": "Leonardo"]])
        ExposeBox.shared.realImpression([item], onSuccess: {
            success = true
            expectation.fulfill()
        }, onFailed: { error in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2.0, handler: nil)
        XCTAssertEqual(true, success)
    }
}
