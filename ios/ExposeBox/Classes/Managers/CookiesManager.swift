//
//  CookiesManager.swift
//  ExposeBoxTest
//
//  Created by Evgeniy on 8/10/20.
//

import Foundation

protocol CookiesManagerProtocol {
    func saveCookies(_ cookies: [HTTPCookie], fromUrl: String)
    func fetchCookies() -> ([HTTPCookie]?, String?)
}

class CookiesManager: CookiesManagerProtocol {
    private let dataManager: DataManagerProtocol

    init(dataManager: DataManagerProtocol) {
        self.dataManager = dataManager
    }

    func saveCookies(_ cookies: [HTTPCookie], fromUrl: String) {
        var cookiesData: [Data] = []
        for cookie in cookies {
            if let data = cookie.archive() {
                cookiesData.append(data)
            }
        }
        dataManager.update(cookies: cookiesData, fromUrl: fromUrl)
    }

    func fetchCookies() -> ([HTTPCookie]?, String?) {
        if let cookiesData = dataManager.cookies {
            var cookiesArray: [HTTPCookie] = []
            for cookieData in cookiesData {
                if let cookie = HTTPCookie.loadCookie(using: cookieData) {
                    cookiesArray.append(cookie)
                }
            }
            let fromUrl = dataManager.cookiesFromUrl
            return (cookiesArray, fromUrl)
        }
        return (nil, nil)
    }
}
