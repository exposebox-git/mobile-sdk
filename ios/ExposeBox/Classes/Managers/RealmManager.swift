//
//  RealmManager.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import RealmSwift

public protocol RealmManagerProtocol {
    func save(object: Object, completion completionBlock: (() -> Void)?)
    func delete(object: Object, completion completionBlock: (() -> Void)?)
    func read(completion completionBlock: ((Results<Event>) -> Void)?)
    func setup(configuration: Realm.Configuration?)
    func deleteAll()
}

// MARK: - DatabaseWrapper
public class RealmManager: RealmManagerProtocol {
    private let writeDispatchQueue: DispatchQueue
    private let realm: Realm

    init() {
        self.writeDispatchQueue =  DispatchQueue(label: "RealmManager.write.queue")
        if let _ = NSClassFromString("XCTest") {
            do {
                self.realm = try Realm(configuration: Realm.Configuration(inMemoryIdentifier: "test", encryptionKey: nil, readOnly: false, schemaVersion: 0, migrationBlock: nil, objectTypes: nil))
            } catch {
                fatalError("Cannot create Realm with error: \(error)")
            }

        } else {
            do {
                self.realm = try Realm()
            } catch {
                fatalError("Cannot create Realm with error: \(error)")
            }
        }
    }

    public func setup(configuration: Realm.Configuration?) {
        let config = Realm.Configuration(schemaVersion: 0, migrationBlock: { (migration: Migration, oldSchemaVersion: UInt64) in
            fatalError("Executing empty migration Block for old SchemeVersion: \(oldSchemaVersion)")
        })

        Realm.Configuration.defaultConfiguration = (configuration ?? config)
    }

    public func deleteAll() {
        write { (realm: Realm) in
            realm.deleteAll()
        }
    }

    public func delete(object: Object, completion completionBlock: (() -> Void)?) {
        write { (realm: Realm) in
            realm.delete(object)
            completionBlock?()
        }
    }

    public func save(object: Object, completion completionBlock: (() -> Void)?) {
        write { (realm: Realm) in
            realm.add(object)
            DispatchQueue.main.async {
                completionBlock?()
            }
        }
    }

    public func read(completion completionBlock: ((Results<Event>) -> Void)?) {
        fetch { (results: Results<Event>) in
            DispatchQueue.main.async {
                completionBlock?(results)
            }
        }
    }

    // MARK: - Dispatch Sync Write Block
    private func write(block: ((_ realm: Realm) -> Void)) {
        self.writeDispatchQueue.sync {
            do {
                try realm.write {
                    block(realm)
                }
            } catch {
                fatalError("Cannot write in realm with error: \(error)")
            }
        }
    }

    private func fetch(block: ((_ results: Results<Event>) -> Void)) {
        let results: Results<Event> = realm.objects(Event.self)
        block(results)
    }
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for index in 0 ..< count {
            if let result = self[index] as? T {
                array.append(result)
            }
        }

        return array
    }
}
