//
//  OfflineManager.swift
//  ExposeBoxTest
//
//  Created by Evgeniy on 8/10/20.
//

import Foundation
import RealmSwift
import Alamofire

protocol OfflineManagerProtocol {
    func save(event: Event)
    func sendStoredEvents()
}

class OfflineManager {
    private let realmManager: RealmManagerProtocol
    private let delayAPIService: DelayAPIServiceProtocol
    private var processingEvents = [Event]()
    private var sessionManager: SessionManagerProtocol

    init(realmManager: RealmManagerProtocol, delayAPIService: DelayAPIServiceProtocol, sessionManager: SessionManagerProtocol) {
        self.realmManager = realmManager
        self.delayAPIService = delayAPIService
        self.sessionManager = sessionManager
        self.sessionManager.delegate = self
    }

    private func removeProcessingEvent(event: Event) {
        guard let index = processingEvents.firstIndex(where: { $0.id == event.id }) else { return }
        processingEvents.remove(at: index)
    }
}

extension OfflineManager: OfflineManagerProtocol {
    func save(event: Event) {
        if !processingEvents.contains(event) {
            realmManager.save(object: event, completion: nil)
        }
    }

    func sendStoredEvents() {
        realmManager.read(completion: { storedEvents in
            self.processingEvents = storedEvents.toArray(ofType: Event.self)
            for event in storedEvents {
                self.delayAPIService.sendEvent(event: event, onSuccess: {
                    self.removeProcessingEvent(event: event)
                    self.realmManager.delete(object: event, completion: nil)
                }, onError: { error in
                    self.removeProcessingEvent(event: event)
                })
            }
        })
    }
}

extension OfflineManager: SessionManagerDelegate {
    func didSuccess(for request: URLRequest) {
        if processingEvents.isEmpty {
            sendStoredEvents()
        }
    }
}
