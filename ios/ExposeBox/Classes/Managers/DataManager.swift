//
//  DataManager.swift
//  ExposeBoxTest
//
//  Created by Evgeniy on 8/10/20.
//

import Foundation

protocol DataManagerProtocol: AnyObject {
    var cookies: [Data]? { get }
    var cookiesFromUrl: String? { get }
    var userId: String? { get }
    var startTimestamp: Date? { get }
    func update(cookies: [Data], fromUrl: String)
    func updateUserId(userId: String)
    func updateStartTimestamp(time: Date)
}

@propertyWrapper struct UserDefaultsBacked<Value> {
    let key: String
    var storage: UserDefaults = .standard

    var wrappedValue: Value? {
        get { storage.value(forKey: key) as? Value }
        set { storage.setValue(newValue, forKey: key) }
    }
}

class DataManager: DataManagerProtocol {
    @UserDefaultsBacked(key: "cookies") var cookies: [Data]?
    @UserDefaultsBacked(key: "userId") var userId: String?
    @UserDefaultsBacked(key: "cookiesFromUrl") var cookiesFromUrl: String?
    @UserDefaultsBacked(key: "startTimestamp") var startTimestamp: Date?

    func update(cookies: [Data], fromUrl: String) {
        self.cookies = cookies
        self.cookiesFromUrl = fromUrl
    }

    func updateUserId(userId: String) {
        self.userId = userId
    }

    func updateStartTimestamp(time: Date) {
        self.startTimestamp = time
    }
}
