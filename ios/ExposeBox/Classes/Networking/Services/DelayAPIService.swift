//
//  DelayAPIService.swift
//  DemoApp
//
//  Created by Evgeniy on 8/11/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

protocol DelayAPIServiceProtocol {
    func sendEvent(event: Event, onSuccess: @escaping () -> Void, onError: APIErrorCompletion?)
    func sendEventWithReponse(event: Event, onSuccess: @escaping (RecommendationsResponse) -> Void, onError: APIErrorCompletion?)
    var isSendingEnable: Bool { get set }
}

class DelayAPIService: DelayAPIServiceProtocol {
    private let apiService: APIServiceProtocol
    var isSendingEnable = true

    init(apiService: APIServiceProtocol) {
        self.apiService = apiService
    }

    func sendEvent(event: Event, onSuccess: @escaping () -> Void, onError: APIErrorCompletion?) {
        if !isSendingEnable {
            print("Sending disabled (GDPR or COOPPA enabled)")
            return
        }
        let request = DelayAPIRequest.sendEvent(event)
        apiService.sendRequest(request, onSuccess: onSuccess, onError: onError)
    }

    func sendEventWithReponse(event: Event, onSuccess: @escaping (RecommendationsResponse) -> Void, onError: APIErrorCompletion?) {
        if !isSendingEnable {
            print("Sending disabled (GDPR or COOPPA enabled)")
            return
        }
        let request = DelayAPIRequest.sendEvent(event)
        apiService.sendDecodableRequest(request, onSuccess: onSuccess, onError: onError)
    }
}

// MARK - DelayAPIRequest

public enum DelayAPIRequest: APIRequestConvertible {
    case sendEvent(Event)

    func asAPIRequest() throws -> APIRequest {
        switch self {
        case .sendEvent(let event):
            let encoding: ParameterEncoding = event.method == .get ? URLEncoding.default : JSONEncoding.default
            let params = event.paramsData.toParams()
            return APIRequest(type: event.type, method: event.method, encoding: encoding, parameters: params)
        }
    }
}


