//
//  APIError.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

/// Base cases of api errors that need to be handled
enum APIError: Error {
    case network(Error)
    case other(Error)
    case requestTimeOut(Error)

    init(fromError error: AFError, responseData: Data?) {
        if let urlError = error.underlyingError as? URLError {
            let code = urlError.code
            if code == .notConnectedToInternet || code == .networkConnectionLost {
                self = .network(error)
            } else if code == .timedOut {
                self = .requestTimeOut(error)
            } else {
                self = .other(error)
            }
        } else {
            self = .other(error)
        }
    }
}
