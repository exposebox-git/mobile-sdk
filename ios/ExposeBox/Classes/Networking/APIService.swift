//
//  APIService.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

typealias DefaultCompletion = () -> ()
typealias PermissionCompletion = (Bool) -> ()


protocol APIServiceProtocol {
    /// Used for base requests without serialization
    func sendRequest(_ request: APIRequestConvertible, onSuccess: DefaultCompletion?, onError: APIErrorCompletion?)
    /// Used when response data must not be empty and serialization is required
    func sendDecodableRequest<T: Decodable>(_ request: APIRequestConvertible,
                                            dateDecodingStrategy: JSONDecoder.DateDecodingStrategy,
                                            onSuccess: @escaping (T) -> Void,
                                            onError: APIErrorCompletion?)
    /// Used when response data may be empty
    func sendOptionalDecodableRequest<T: Decodable>(_ request: APIRequestConvertible,
                                                    dateDecodingStrategy: JSONDecoder.DateDecodingStrategy,
                                                    onSuccess: @escaping (T?) -> Void,
                                                    onError: APIErrorCompletion?)
}

/// Base class for API requests wrapper around SessionManager. Introduces request forming and serialization
class APIService: APIServiceProtocol {
    private let sessionManager: SessionManagerProtocol
    private let requestFactory: URLRequestFactoryProtocol

    init(sessionManager: SessionManagerProtocol, requestFactory: URLRequestFactoryProtocol) {
        self.sessionManager = sessionManager
        self.requestFactory = requestFactory
    }

    // MARK: - Serializing

    private func decode<T: Decodable>(data: Data,
                                      withDecoder decoder: JSONDecoder,
                                      onSuccess: (T) -> Void,
                                      onError: APIErrorCompletion?) {
        do {
            let value = try decoder.decode(T.self, from: data)
            onSuccess(value)
        } catch {
            onError?(.other(error))
        }
    }


    // MARK: - Requests

    public func sendRequest(_ request: APIRequestConvertible, onSuccess: DefaultCompletion?, onError: APIErrorCompletion?) {
        let request = requestFactory.urlRequest(from: request)
        sessionManager.sendRequest(request, onSuccess: { _ in
            onSuccess?()
        }, onError: onError)
    }

    public func sendDecodableRequest<T: Decodable>(_ request: APIRequestConvertible,
                                                   dateDecodingStrategy: JSONDecoder.DateDecodingStrategy,
                                                   onSuccess: @escaping (T) -> Void,
                                                   onError: APIErrorCompletion?) {

        let request = requestFactory.urlRequest(from: request)

        sessionManager.sendRequest(request, onSuccess: { data in
            guard let data = data else {
                let error = AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength)
                onError?(.other(error))
                return
            }
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = dateDecodingStrategy
            self.decode(data: data, withDecoder: decoder, onSuccess: onSuccess, onError: onError)
        }, onError: onError)
    }

    public func sendOptionalDecodableRequest<T: Decodable>(_ request: APIRequestConvertible,
                                                           dateDecodingStrategy: JSONDecoder.DateDecodingStrategy,
                                                           onSuccess: @escaping (T?) -> Void,
                                                           onError: APIErrorCompletion?) {

        let request = requestFactory.urlRequest(from: request)

        sessionManager.sendRequest(request, onSuccess: { data in
            guard let data = data, data.count > 0 else {
                onSuccess(nil)
                return
            }

            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = dateDecodingStrategy
            self.decode(data: data, withDecoder: decoder, onSuccess: onSuccess, onError: onError)
        }, onError: onError)
    }
}

// MARK: - Helper Methods to provide default arguments to protocol methods

extension APIServiceProtocol {
    func sendDecodableRequest<T: Decodable>(_ request: APIRequestConvertible,
                                            onSuccess: @escaping (T) -> Void,
                                            onError: APIErrorCompletion?) {

        sendDecodableRequest(request, dateDecodingStrategy: .millisecondsSince1970, onSuccess: onSuccess, onError: onError)
    }
}
