//
//  SessionManager.swift
//  ExposeBoxTest
//
//  Created by Evgeniy on 8/10/20.
//

import Foundation
import Alamofire

typealias ResponseDataCompletion = (Data?) -> Void
typealias APIErrorCompletion = (APIError) -> Void
typealias ProgressHandler = (Progress) -> Void
typealias DefaultDataResponse = AFDataResponse<Data?>

/// All requests may or may not return data. Handling it is responsibility of seriliazation classes.
protocol SessionManagerProtocol {
    /// Used for base requests
    func sendRequest(_ request: URLRequest, onSuccess: @escaping ResponseDataCompletion, onError: APIErrorCompletion?)
    var delegate: SessionManagerDelegate? { get set }
}

protocol SessionManagerDelegate: class {
    func didSuccess(for request: URLRequest)
}

/// Wrapper around Alamofire SessionManager to reduce complexity and simplify migration if needed
class SessionManager: SessionManagerProtocol {
    private let sessionManager: Alamofire.Session
    private let cookiesManager: CookiesManagerProtocol
    public var delegate: SessionManagerDelegate?

    init(cookiesManager: CookiesManagerProtocol) {
        self.sessionManager = Alamofire.Session(configuration: .default)
        self.cookiesManager = cookiesManager
        setupCookies()
    }

    // MARK: - Response Handling

    private func process(response: DefaultDataResponse, request: URLRequest, onSuccess: ResponseDataCompletion, onError: APIErrorCompletion?) {
        if let allHeaderFields = response.response?.allHeaderFields,
            let allHeaderFieldsTuple = allHeaderFields as? [String: String],
            let url = response.response?.url {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFieldsTuple, for: url)
            cookiesManager.saveCookies(cookies, fromUrl: url.absoluteString)
            sessionManager.sessionConfiguration.httpCookieStorage?.setCookies(cookies, for: url, mainDocumentURL: nil)
        }
        if let error = response.error {
            let apiError = APIError(fromError: error, responseData: response.data)
            onError?(apiError)
        } else {
            delegate?.didSuccess(for: request)
            onSuccess(response.data)
        }
    }

    // MARK: - Response Handling
    private func setupCookies() {
        let (cookies, fromUrl) = cookiesManager.fetchCookies()
        if let cookies = cookies, let urlString = fromUrl, let url = URL(string: urlString) {
            sessionManager.sessionConfiguration.httpCookieStorage?.setCookies(cookies, for: url, mainDocumentURL: nil)
        }
    }

    // MARK: - Requests

    func sendRequest(_ request: URLRequest, onSuccess: @escaping ResponseDataCompletion, onError: APIErrorCompletion?) {
        sessionManager.request(request).validate().response { response in
            self.process(response: response, request: request, onSuccess: onSuccess, onError: onError)
        }
    }
}
