//
//  URLRequestFactory.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import AdSupport

protocol APIRequestConvertible {
    func asAPIRequest() throws -> APIRequest
}

struct APIRequest {
    let type: RequestType?
    let method: Alamofire.HTTPMethod
    let encoding: ParameterEncoding?
    let parameters: Any?
    let headers: HTTPHeaders?

    init(type: RequestType? = nil,
         method: Alamofire.HTTPMethod,
         encoding: ParameterEncoding? = nil,
         parameters: Any? = nil,
         headers: HTTPHeaders? = nil) {

        self.type = type
        self.method = method
        self.encoding = encoding
        self.parameters = parameters
        self.headers = headers
    }
}


protocol URLRequestFactoryProtocol {
    func urlRequest(from apiRequest: APIRequestConvertible) -> URLRequest
    var companyId: String? { get set }
}

public struct RequestType: RawRepresentable {
    public static let view = RequestType(rawValue: "view")
    public static let event = RequestType(rawValue:"event")
    public static let clickEvent = RequestType(rawValue:"event/click")
    public static let recommendations = RequestType(rawValue:"item-recommendations")

    public let rawValue: String

    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

/// Initialized with host and then, based on it, creates URLRequestsobject: Object,
class URLRequestFactory: URLRequestFactoryProtocol {
    let host: URL
    var companyId: String?
    var appId: String?

    private var baseURLString: String {
        return "px/\(companyId ?? "")/"
    }
    private var recommendationsURLString: String {
        return "api/"
    }

    lazy var userAgent: String? = {
        return userAgentString()
    }()

    private var initialInfoUploaded = false

    private var advertiserId: String? {
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            return ASIdentifierManager.shared().advertisingIdentifier.uuidString
        } else {
            return nil
        }
    }

    init(host: URL) {
        self.host = host
    }

    func urlRequest(from apiRequest: APIRequestConvertible) -> URLRequest {
        let requestConfig: APIRequest
        do {
            requestConfig = try apiRequest.asAPIRequest()
        } catch let error {
            fatalError("Provided request configuration is wrong: \(error)")
        }

        var url: URL

        if let type = requestConfig.type {
            let requestPath = path(for: type)
            url = host.appendingPathComponent(requestPath)
        } else {
            url = host
        }

        var request = URLRequest(url: url)
        request.httpMethod = requestConfig.method.rawValue
        request.addHeaders(requestConfig.headers ?? [:])
        request.httpShouldHandleCookies = true
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")


        if let data = requestConfig.parameters as? Data {
            request.addParameters(data)
            request.addParameters(Data("\(arc4random())".utf8))
        } else if let encoding = requestConfig.encoding, var parameters = requestConfig.parameters as? Parameters {
            parameters.updateValue("\(arc4random())", forKey: "_")
            var formatedParams: [String: Any] = parameters
            if requestConfig.method == .get {
                let params = parameters.urlQuerryItems(prefix: "", top: true)
                formatedParams = params.reduce(into: [:]) {
                    params, queryItem in
                    params[queryItem.name] = queryItem.value
                }
            }

            do {
                try request.addParameters(formatedParams, encoding: encoding)
            } catch let error {
                fatalError("Request parameters configuration is wrong: \(error)")
            }
        }

        return request
    }

    private func path(for requestType: RequestType) -> String {
        if requestType == .recommendations {
            return recommendationsURLString + requestType.rawValue
        } else {
            return baseURLString + requestType.rawValue
        }
    }

    private func DarwinVersion() -> String {
        var sysinfo = utsname()
        uname(&sysinfo)
        let dv = String(bytes: Data(bytes: &sysinfo.release, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
        return "Darwin/\(dv)"
    }

    private func CFNetworkVersion() -> String {
        guard let dictionary = Bundle(identifier: "com.apple.CFNetwork")?.infoDictionary else {
            return "none"
        }
        guard let version = dictionary["CFBundleShortVersionString"] as? String else {
            return "none"
        }
        return "CFNetwork/\(version)"
    }

    private func deviceVersion() -> String {
        let currentDevice = UIDevice.current
        return "\(currentDevice.systemName)/\(currentDevice.systemVersion)"
    }

    private func deviceName() -> String {
        var sysinfo = utsname()
        uname(&sysinfo)
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }

    private func appNameAndVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        guard let version = dictionary["CFBundleShortVersionString"] as? String else {
            return "none"
        }
        guard let name = dictionary["CFBundleName"] as? String else {
            return "none"
        }
        return "\(name)/\(version)"
    }

    private func userAgentString() -> String {
        return "\(appNameAndVersion()) \(deviceName()) \(deviceVersion()) \(CFNetworkVersion()) \(DarwinVersion())"
    }
}
