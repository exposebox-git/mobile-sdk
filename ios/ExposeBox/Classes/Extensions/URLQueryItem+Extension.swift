//
//  URLQueryItem+Extension.swift
//  Alamofire
//
//  Created by Evgeniy on 8/18/20.
//

import Foundation

public extension Dictionary {
    func urlQuerryItems(prefix: String, top: Bool = false) -> [URLQueryItem] {
        var result: [URLQueryItem] = []
        for (key, value) in self {
            let key = top ? "\(key)" : (prefix + "[\(key)]")
            if let element = value as? [Any] {
                result.append(contentsOf: element.urlQuerryItems(prefix: key))
            } else if let element = value as? [String: Any] {
                result.append(contentsOf: element.urlQuerryItems(prefix: key))
            } else if let string = value as? String {
                result.append(URLQueryItem(name: key, value: string))
            } else if let value = value as? Int {
                result.append(URLQueryItem(name: key, value: "\(value)"))
            } else if let value = value as? Double {
                result.append(URLQueryItem(name: key, value: "\(value)"))
            }
        }
        return result
    }
}

public extension Array {
    func urlQuerryItems(prefix: String) -> [URLQueryItem] {
        var result: [URLQueryItem] = []
        for index in 0..<count {
            let key = prefix + "[\(index)]"
            if let element = self[index] as? [Any] {
                result.append(contentsOf: element.urlQuerryItems(prefix: key))
            } else if let element = self[index] as? [String: Any] {
                result.append(contentsOf: element.urlQuerryItems(prefix: key))
            } else if let string = self[index] as? String {
                result.append(URLQueryItem(name: key, value: string))
            } else if let value = self[index] as? Int {
                result.append(URLQueryItem(name: key, value: "\(value)"))
            } else if let value = self[index] as? Double {
                result.append(URLQueryItem(name: key, value: "\(value)"))
            }
        }

        return result
    }
}
