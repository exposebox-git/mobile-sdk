//
//  URLRequest+Extension.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequest {
    mutating func addHeaders(_ headers: HTTPHeaders) {
        for header in headers {
            addValue(header.value, forHTTPHeaderField: header.name)
        }
    }

    mutating func addParameters(_ parameters: Parameters, encoding: ParameterEncoding) throws {
        self = try encoding.encode(self, with: parameters)
    }

    mutating func addParameters(_ parameters: Data) {
        httpBody = parameters
        let jsonHeader = HTTPHeader(name: "Content-Type", value: "application/json")
        addHeaders([jsonHeader])
    }
}
