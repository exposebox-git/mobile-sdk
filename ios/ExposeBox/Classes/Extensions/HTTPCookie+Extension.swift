//
//  HTTPCookie + Extension.swift
//  ExposeBoxTest
//
//  Created by Evgeniy on 8/10/20.
//
import Foundation

extension HTTPCookie {
    fileprivate func save(cookieProperties: [HTTPCookiePropertyKey : Any]) -> Data {
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: cookieProperties, requiringSecureCoding: true)
            return data
        } catch let error {
            fatalError("NSKeyedArchiver: \(error)")
        }
    }

    static fileprivate func loadCookieProperties(from data: Data) -> [HTTPCookiePropertyKey : Any]? {
        do {
            let unarchivedDictionary = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
            return unarchivedDictionary as? [HTTPCookiePropertyKey : Any]
        } catch let error {
            fatalError("NSKeyedArchiver: \(error)")
        }
    }

    static func loadCookie(using data: Data?) -> HTTPCookie? {
        guard let data = data,
            let properties = loadCookieProperties(from: data) else {
                return nil
        }
        return HTTPCookie(properties: properties)

    }

    func archive() -> Data? {
        guard let properties = self.properties else {
            return nil
        }
        return save(cookieProperties: properties)
    }

}
