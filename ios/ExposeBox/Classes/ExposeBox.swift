import Foundation
import AdSupport

enum Constants {
    static let baseURLString =  "https://server.exposebox.com/"
}

public typealias CompletionBlock = () -> ()

public class ExposeBox {
    public static var shared: ExposeBox = {
        let instance = ExposeBox()
        return instance
    }()

    private var delayAPIService: DelayAPIServiceProtocol
    private var companyId: String? {
        didSet {
            requestFactory.companyId = companyId
        }
    }
    private var appId: String?
    private var advertiserId: String? {
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            return ASIdentifierManager.shared().advertisingIdentifier.uuidString
        } else {
            return nil
        }
    }
    private var startTimestamp = Date()
    private var savedStartTimestamp: Date?
    private var userId: String?
    public var hasCoppaCompliance: Bool = false {
        didSet {
            delayAPIService.isSendingEnable = hasCoppaCompliance && hasGDPR
        }
    }
    public var hasGDPR: Bool = false {
        didSet {
            delayAPIService.isSendingEnable = hasCoppaCompliance && hasGDPR
        }
    }
    private let offlineManager: OfflineManagerProtocol
    private var requestFactory: URLRequestFactoryProtocol
    private let dataManager: DataManagerProtocol
    private let cookiesManager: CookiesManagerProtocol

    private init() {
        let baseUrl = URL(string: Constants.baseURLString)!
        requestFactory = URLRequestFactory(host: baseUrl)
        dataManager = DataManager()
        cookiesManager = CookiesManager(dataManager: dataManager)
        let sessionManager = SessionManager(cookiesManager: cookiesManager)
        let apiService = APIService(sessionManager: sessionManager, requestFactory: requestFactory)

        delayAPIService = DelayAPIService(apiService: apiService)
        let realmManager = RealmManager()
        self.offlineManager = OfflineManager(realmManager: realmManager, delayAPIService: delayAPIService, sessionManager: sessionManager)
        self.userId = dataManager.userId
    }

    public func resetSession() {
        startTimestamp = Date()
    }

    public func configure(companyId: String, appId: String, coppaCompliance: Bool, GDPR: Bool) {
        self.companyId = companyId
        self.appId = appId
        self.hasCoppaCompliance = coppaCompliance
        self.hasGDPR = GDPR
    }

    private func sendGeneralInfo(onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["userId" : userId ?? "",
                                    "appId": appId ?? "",
                                    "idfa": advertiserId ?? "",
                                    "viewName": "MainActivity"])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func addToCart(productId: String, quantity: Int, unitPrice: Double, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["addToCart": ["productId": productId,
                                                  "quantity": quantity,
                                                  "unitPrice": unitPrice]])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func removeFromCart(productId: String, quantity: Int, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["removeFromCart": ["productId": productId,
                                                       "quantity": quantity]])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func recommendations(placementIds: [String], onSuccess: @escaping ((RecommendationsResponse) -> Void), onFailed: ((Error)->())?) {
        guard let companyId = self.companyId else { return }
        let params = Params(value: ["companyId": companyId, "placementIds": placementIds])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .post, type: .recommendations, paramsData: paramsData)
        sendEventWithResponse(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func setCategories(categoriesNames: [String], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["categories": categoriesNames])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func view(viewName: String, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let sessionDuration = Int(startTimestamp.timeIntervalSinceNow * 1000)
        let params = Params(value: ["viewName": viewName,
                                    "sessionDuration": sessionDuration])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func setTags(tags: [String: [String]], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["tags": tags])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func setCustomerData(customerData: ExposeBoxCustomerData, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["customer": customerData.dictionary])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func conversion(orderId: String, totalPrice: Double, cartProducts: [ExposeBoxCartProduct], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {

        let params = Params(value: ["conversion": ["orderId": orderId,
                                                   "totalPrice": totalPrice,
                                                   "cartProducts": cartProducts.map { $0.dictionary }]])

        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func setProducts(productIds: [String], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["products": productIds])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .view, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func customEvent(name: String, data: [String: Any], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: [name: data])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func click(placementId: String, widgetId: Int, productId: String, additionalData: [String: String]?, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        var arguments = ["placementId": placementId,
                         "widgetId": widgetId,
                         "productId": productId] as [String : Any]
        if let data = additionalData {
            arguments.updateValue(data, forKey: "additionalData")
        }
        let params = Params(value: ["click": arguments])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func addToWishlist(productId: String, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["addToWishlist": ["productId": productId]])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func removeFromWishlist(productId: String, onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["removeFromWishlist": ["productId": productId]])
        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    public func realImpression(_ batches: [ExposeBoxRealImpressionBatch], onSuccess: CompletionBlock?, onFailed: ((Error)->())?) {
        let params = Params(value: ["realImpression": batches.map { $0.dictionary }])

        guard let paramsData = params.toData() else { return }
        let event = Event(method: .get, type: .event, paramsData: paramsData)
        sendEvent(event: event, onSuccess: onSuccess, onFailed: onFailed)
    }

    private func sendEvent(event: Event, onSuccess: CompletionBlock?, onFailed: APIErrorCompletion?) {
        delayAPIService.sendEvent(event: event, onSuccess: {
            onSuccess?()
            let (cookies, _) = self.cookiesManager.fetchCookies()
            if let userId = cookies?.filter({ $0.name == "__etn" }).first?.value {
                self.dataManager.updateUserId(userId: userId)
                self.userId = userId
            }
            self.sendGeneralInfo()
        }, onError: { (error) in
            self.offlineManager.save(event: event)
            onFailed?(error)
        })
    }

    private func sendEventWithResponse(event: Event, onSuccess: @escaping (RecommendationsResponse) -> Void?, onFailed: APIErrorCompletion?) {
        delayAPIService.sendEventWithReponse(event: event, onSuccess: { response in
            onSuccess(response)
        }, onError: { (error) in
            self.offlineManager.save(event: event)
            onFailed?(error)
        })
    }

    private func sendGeneralInfo() {
        if userId == nil { return }
        if let savedStartTimestamp = dataManager.startTimestamp {
            self.savedStartTimestamp = savedStartTimestamp
            let startDay = Calendar.current.dateComponents([.day], from: startTimestamp)
            let savedDay = Calendar.current.dateComponents([.day], from: savedStartTimestamp)
            if startDay != savedDay {
                dataManager.updateStartTimestamp(time: startTimestamp)
                sendGeneralInfo(onSuccess: nil, onFailed: nil)
            }
        } else {
            dataManager.updateStartTimestamp(time: startTimestamp)
            sendGeneralInfo(onSuccess: nil, onFailed: nil)
        }
    }
}
