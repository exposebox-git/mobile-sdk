//
//  Params.swift
//  DemoApp
//
//  Created by Evgeniy on 8/12/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation

public struct Params: Codable {
    let value: [String: Any]

    enum CodingKeys: String, CodingKey {
        case value
    }

    public init(value: [String: Any]) {
        self.value = value
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let data = try values.decode(Data.self, forKey: .value)
        value = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        let data = try JSONSerialization.data(withJSONObject: value, options: [])
        try container.encode(data, forKey: .value)
    }

    public func toData() -> Data? {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(self)
            return data
        } catch {
            return nil
        }
    }
}

extension Data {
    func toParams() -> [String: Any]? {
        let decoder = JSONDecoder()
        do {
            let params = try decoder.decode(Params.self, from: self)
            return params.value
        } catch {
            return nil
        }
    }
}
