//
//  Event.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

public class Event: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc private dynamic var _method: Alamofire.HTTPMethod.RawValue = ""
    public var method: Alamofire.HTTPMethod {
        get { return Alamofire.HTTPMethod.init(rawValue: _method) }
        set { _method = newValue.rawValue }
    }
    @objc private dynamic var _type: RequestType.RawValue = ""
    public var type: RequestType {
        get { return RequestType(rawValue: _type) }
        set { _type = newValue.rawValue }
    }
    @objc dynamic var paramsData: Data = Data()

    public convenience init(method: Alamofire.HTTPMethod, type: RequestType, paramsData: Data) {
        self.init()
        self.method = method
        self.type = type
        self.paramsData = paramsData
    }
}

