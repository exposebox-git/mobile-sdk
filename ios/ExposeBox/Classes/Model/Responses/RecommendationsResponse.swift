//
//  RecomendationsResoinse.swift
//  DemoApp
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation

public struct RecommendationsResponse: Codable {
    public let userId: Int?
    public let iftags: Bool?
    public let placements: [String: Placement]?
    public let slu: Int?
}

public struct Placement: Codable {
    public let placementId: String?
    public let abt: Int?
    public let modelConfigId: Int?
    public let layoutId: Int?
    public let widgetId: Int?
    public let layoutUpdateTime: Int?
    public let products: [Product]?
}

public struct Product: Codable {
    public let id: String?
    public let companyId: Int?
    public let saleOpen: Bool?
    public let title: String?
    public let description: String?
    public let brand: String?
    public let category: String?
    public let price: Double?
    public let url: String?
    public let freeData: [String: String]?
    public let onSale: Bool?
    public let categoryBreadcrumbs: [String]
    public let imageUrl: String?
}
