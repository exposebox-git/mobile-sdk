//
//  ExposeBoxCustomerData.swift
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import Foundation

/// Data container for Customer data
public class ExposeBoxCustomerData: NSObject {
    public var email: String
    var customerId: String
    public var firstName: String?
    public var lastName: String?
    public var address1: String?
    public var address2: String?
    public var city: String?
    public var country: String?
    public var state: String?
    public var zipCode: String?
    public var phone: String?
    public var freeData: [String: String]?

    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case email
        case customerId
        case address1
        case address2
        case city
        case country
        case state
        case zipCode
        case phone
        case freeData
    }

    public init(email: String, customerId: String) {
        self.email = email
        self.customerId = customerId
        super.init()
    }

    public var dictionary: [String: Any] {
        var dict = [String: Any]()

        dict.updateValue(email, forKey: CodingKeys.email.rawValue)
        dict.updateValue(customerId, forKey: CodingKeys.customerId.rawValue)
        if let firstName = firstName {
            dict.updateValue(firstName, forKey: CodingKeys.firstName.rawValue)
        }
        if let lastName = lastName {
            dict.updateValue(lastName, forKey: CodingKeys.lastName.rawValue)
        }
        if let address1 = address1 {
            dict.updateValue(address1, forKey: CodingKeys.address1.rawValue)
        }
        if let address2 = address2 {
            dict.updateValue(address2, forKey: CodingKeys.address2.rawValue)
        }
        if let city = city {
            dict.updateValue(city, forKey: CodingKeys.city.rawValue)
        }
        if let country = country {
            dict.updateValue(country, forKey: CodingKeys.country.rawValue)
        }
        if let state = state {
            dict.updateValue(state, forKey: CodingKeys.state.rawValue)
        }
        if let phone = phone {
            dict.updateValue(phone, forKey: CodingKeys.phone.rawValue)
        }
        if let zipCode = zipCode {
            dict.updateValue(zipCode, forKey: CodingKeys.zipCode.rawValue)
        }
        if let freeData = freeData {
            dict.updateValue(freeData, forKey: CodingKeys.freeData.rawValue)
        }
        return dict
    }
}
