//
//  ExposeBoxRealImpressionBatch.swift
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

import UIKit

/// Batch object to group real impression objects
public struct ExposeBoxRealImpressionBatch: Codable {
    /// Identifier of placement
    let placementId: String
    /// Identifier of widget
    let widgetId: Int
    /// Items key value pairs
    let items: [[String: String]]?

    /**
        Batch object to group real impression objects
        - Parameter placementId: Identifier of placement
        - Parameter widgetId: Identifier of widget
        - Parameter items: Items key value pairs
    */
    enum CodingKeys: String, CodingKey {
      case placementId
      case widgetId
      case items
    }

    public init(placementId: String, widgetId: Int, items: [[String: String]]?) {
        self.placementId = placementId
        self.widgetId = widgetId
        self.items = items
    }

    public var dictionary: [String: Any] {
        return [CodingKeys.placementId.rawValue: placementId,
                CodingKeys.widgetId.rawValue: widgetId,
                CodingKeys.items.rawValue: items ?? ""]
    }
}
