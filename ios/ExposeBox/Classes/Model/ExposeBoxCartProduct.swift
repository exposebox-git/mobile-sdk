//
//  ExposeBoxCartProduct.swift
//
//  Created by Evgeniy on 8/7/20.
//  Copyright © 2020 Evgeniy. All rights reserved.
//

public struct ExposeBoxCartProduct: Encodable {
    /// Identifier of product
    let productId: String
    /// Quantity of products
    let quantity: Int
    /// Price per Unit
    let unitPrice: Double
    /**
     Object for cart product representation
     - Parameter productId: Identifier of product
     - Parameter quantity: Quantity of products
     - Parameter unitPrice: Price per Unit
     */

    enum CodingKeys: String, CodingKey {
      case productId
      case quantity
      case unitPrice
    }

    public init(productId: String, quantity: Int, unitPrice: Double) {
        self.productId = productId
        self.quantity = quantity
        self.unitPrice = unitPrice
    }

    public var dictionary: [String: Any] {
        return [CodingKeys.productId.rawValue: productId,
                CodingKeys.quantity.rawValue: quantity,
                CodingKeys.unitPrice.rawValue: unitPrice]
    }
}
