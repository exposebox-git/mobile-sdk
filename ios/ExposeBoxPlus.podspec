#
# Be sure to run `pod lib lint ExposeBoxTest.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see ExposeBoxTest
#

Pod::Spec.new do |s|
  s.name                  = 'ExposeBoxPlus'
  s.version               = '1.0.0'
  s.summary               = 'ExposeBoxPlus descr'
  s.description           = 'We deliver smart growth marketing that will transform your entire customer lifecycle. Save time and money without the need to juggle multiple platforms and patched-up solutions. Exposebox marketing cloud is architected to maximize customer retention, loyalty, and AOV uplift'
  s.homepage              = 'https://exposebox.com'
  s.license               = { :type => 'MIT' }
  s.author                = { 'ExposeBoxPlus' => 'eran@exposebox.com' }
  s.source                = { :git => 'https://bitbucket.org/exposebox-git/mobile-sdk.git', :tag => s.version.to_s }
  s.pod_target_xcconfig   = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.ios.deployment_target = '11.0'
  s.source_files          = 'ios/ExposeBox/Classes/**/*'
  s.swift_versions        = '5.2'
  s.dependency 'Alamofire', '~> 5.1'
  s.dependency 'RealmSwift'
end