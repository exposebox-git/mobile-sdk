package com.market.exposeboximplementation

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.market.exposebox.network.rest.model.ExposeBoxCartProduct
import com.market.exposebox.network.rest.model.ExposeBoxCustomerData
import com.market.exposebox.network.rest.model.RealImpressionBatch
import com.market.exposebox.sdk.ExposeBox
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addToCart.setOnClickListener {
            ExposeBox.getInstance().addToCart("product-1", 1, 99.0)
        }

        setCustomerData.setOnClickListener {
            ExposeBox.getInstance()
                .setCustomerData(
                    ExposeBoxCustomerData(
                        "test@gmail.com",
                        "2014",
                        "firstName",
                        "lastName",
                        "addr1",
                        "addr2",
                        "city",
                        "county",
                        "state",
                        "zipCode",
                        "phone",
                        mapOf("freedata" to "freedata"),
                        mapOf("freedatapatch" to "freedatapatch")
                    )
                )
        }

        conversion.setOnClickListener {
            val listOfCartProducts = listOf(
                ExposeBoxCartProduct("product-1", 2, 10.0),
                ExposeBoxCartProduct("product-2", 1, 11.0)
            )
            ExposeBox.getInstance()
                .conversion(
                    "99",
                    20.0,
                    listOfCartProducts
                )
        }

        removeFromCart.setOnClickListener {
            ExposeBox.getInstance().removeFromCart("product-1", 2)
        }

        setCateg.setOnClickListener {
            ExposeBox.getInstance().setCategories(listOf("Categ1", "categ2"))
        }

        getRecomm.setOnClickListener {
            ExposeBox.getInstance().recommendations(
                listOf("mainPagePlacement"),
                onSuccess = { recommendationsResponse ->
                    recommendationsResponse?.let { Log.d("TAG", it.toString()) }
                },
                onFailed = {

                }
            )
        }

        setProducts.setOnClickListener {
            ExposeBox.getInstance().setProducts(listOf("product-1", "product-2", "product-3"))
        }

        click.setOnClickListener {
            ExposeBox.getInstance()
                .click("PageView", 2180, "product-7", mapOf("bsrId" to "additionalData"))
        }
        addToWishlist.setOnClickListener {
            ExposeBox.getInstance().addToWishlist("product-1")
        }

        removeFromWishlist.setOnClickListener {
            ExposeBox.getInstance().removeFromWishlist(("product-1"))
        }

        realImpression.setOnClickListener {

            val realImpressionOne = RealImpressionBatch(
                "MyRequestPlacement", 2180, listOf(
                    mapOf(
                        "id" to "gtest12",
                        "itemType" to "product",
                        "catalogId" to "default",
                        "category" to "leonardo-negev-beer-sheba",
                        "brand" to "Leonardo"
                    )
                )
            )

            val realImpressionTwo = RealImpressionBatch(
                "mainPagePlacement", 2180, listOf(
                    mapOf(
                        "id" to "gtest12",
                        "itemType" to "product",
                        "catalogId" to "default",
                        "category" to "leonardo-negev-beer-sheba",
                        "brand" to "Leonardo"
                    )
                )
            )

            val listOfRealImpressions = listOf(realImpressionOne, realImpressionTwo)
            ExposeBox.getInstance().realImpression(listOfRealImpressions)
        }

        customEvent.setOnClickListener {
            ExposeBox.getInstance().customEvent("Custom event", mapOf("testKey" to "TestValue"))
        }

        view.setOnClickListener {
            ExposeBox.getInstance().view("Some view")
        }

        setTags.setOnClickListener {
            ExposeBox.getInstance().setTags(mapOf("someTag" to listOf("Tag1", "Tag2")))
        }
    }
}