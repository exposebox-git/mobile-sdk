package com.market.exposeboximplementation

import android.app.Application
import com.market.exposebox.sdk.ExposeBox

const val TEST_COMPANY_ID = "30510"

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        ExposeBox.configure(
            this,
            TEST_COMPANY_ID,
            "appId",
            coppaCompliance = true,
            gdprCompliance = true
        )
    }
}