package com.market.exposebox.repository

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.market.exposebox.APIMethodType
import com.market.exposebox.RequestMapKeys
import com.market.exposebox.convertFromBytes
import com.market.exposebox.db.RealmManager
import com.market.exposebox.db.RealmManagerImpl
import com.market.exposebox.network.RequestHandlerThread
import com.market.exposebox.network.rest.api.ExposeBoxApi
import com.market.exposebox.network.rest.interceptors.CacheBusterInterceptor
import com.market.exposebox.network.rest.interceptors.MyPersistentCookieJar
import com.market.exposebox.network.rest.interceptors.UserAgentInterceptor
import com.market.exposebox.network.rest.model.*
import com.market.exposeboxsdk.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * A repository that handles all API requests and Database storage
 */

internal class ExposeBoxRepositoryImpl(userAgent: String, context: Context) : ExposeBoxRepository {

    private val exposeBoxApi: ExposeBoxApi
    private val realmManager: RealmManager = RealmManagerImpl()
    private val persistentCookieJar = MyPersistentCookieJar(context)
    private val userAgentInterceptor: UserAgentInterceptor = UserAgentInterceptor(userAgent)
    private val cacheBusterInterceptor: CacheBusterInterceptor = CacheBusterInterceptor()
    private val httpLoggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    private val requestQueue: Queue<Runnable> = LinkedList<Runnable>()
    private val handlerThread = RequestHandlerThread()

    init {
        handlerThread.start()

        val okHttpBuilder = OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .cookieJar(persistentCookieJar)
            .addInterceptor(cacheBusterInterceptor)
            .addNetworkInterceptor(userAgentInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.EXPOSE_BOX_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(okHttpBuilder)
            .build()

        exposeBoxApi = retrofit.create(ExposeBoxApi::class.java)
    }

    override fun addToCart(
        companyId: String,
        productId: String,
        quantity: Int,
        unitPrice: Double,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val mapData = mutableMapOf(
            RequestMapKeys.ADD_TO_CART_PRODUCT_ID.requestMapKey to productId,
            RequestMapKeys.ADD_TO_CART_QUANTITY.requestMapKey to quantity.toString(),
            RequestMapKeys.ADD_TO_CART_UNIT_PRICE.requestMapKey to unitPrice.toString()
        )

        val runnable = Runnable {
            exposeBoxApi.addToCart(companyId, mapData).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun removeFromCart(
        companyId: String,
        productId: String,
        quantity: Int,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val mapData = mutableMapOf(
            RequestMapKeys.REMOVE_FROM_CART_PRODUCT_ID.requestMapKey to productId,
            RequestMapKeys.REMOVE_FROM_CART_QUANTITY.requestMapKey to quantity.toString()
        )

        val runnable = Runnable {
            exposeBoxApi.removeFromCart(companyId, mapData).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun getRecommendations(
        recommendationsRequest: RecommendationsRequest,
        onSuccess: (r: RecommendationsResponse?) -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val runnable = Runnable {
            exposeBoxApi.getRecommendations(recommendationsRequest).enqueue(
                object : Callback<RecommendationsResponse> {
                    override fun onFailure(call: Call<RecommendationsResponse>, t: Throwable) {
                        afterRequestSent()
                        realmManager.saveRequest(
                            call.request().method,
                            call.request().url.toString(),
                            recommendationsRequest
                        )
                        onFailed(t)
                    }

                    override fun onResponse(
                        call: Call<RecommendationsResponse>,
                        response: Response<RecommendationsResponse>
                    ) {
                        onSuccess(response.body())
                        afterRequestSent()
                    }
                })
        }

        addToQueue(runnable)
    }

    override fun setCategories(
        companyId: String,
        categories: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val runnable = Runnable {
            exposeBoxApi.setCategories(companyId, categories).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun view(
        companyId: String,
        viewName: String,
        sessionDuration: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mapOf(
            RequestMapKeys.VIEW_NAME.requestMapKey to viewName,
            RequestMapKeys.SESSION_DURATION.requestMapKey to sessionDuration
        )

        val runnable = Runnable {
            exposeBoxApi.view(companyId, dataMap).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun setTags(
        companyId: String,
        tagMap: Map<String, List<String>>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mutableMapOf<String, Any>()
        tagMap.forEach {
            for (n in it.value.indices) {
                dataMap[RequestMapKeys.TAGS.requestMapKey + "[${it.key}][$n]"] = it.value[n]
            }
        }

        val runnable = Runnable {
            exposeBoxApi.setTags(companyId, dataMap).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun setCustomerData(
        companyId: String,
        data: ExposeBoxCustomerData,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val runnable = Runnable {
            exposeBoxApi.setCustomerData(companyId, data.toMap())
                .enqueue(object : Callback<Unit> {
                    override fun onFailure(call: Call<Unit>, t: Throwable) {
                        afterRequestSent()
                        realmManager.saveRequest(
                            call.request().method,
                            call.request().url.toString(),
                            null
                        )
                        onFailed(t)
                    }

                    override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                        onSuccess()
                        afterRequestSent()
                    }
                })
        }

        addToQueue(runnable)
    }

    override fun conversion(
        companyId: String,
        orderId: String,
        totalPrice: Double,
        cartProducts: List<ExposeBoxCartProduct>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mutableMapOf(
            RequestMapKeys.CONVERSION_ORDER_ID.requestMapKey to orderId,
            RequestMapKeys.CONVERSION_TOTAL_PRICE.requestMapKey to totalPrice
        )

        for (n in cartProducts.indices) {
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.PRODUCT_ID.requestMapKey] =
                cartProducts[n].productId
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.QUANTITY.requestMapKey] =
                cartProducts[n].quantity
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.UNIT_PRICE.requestMapKey] =
                cartProducts[n].unitPrice
        }

        val runnable = Runnable {
            exposeBoxApi.sendConversion(companyId, dataMap).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun setProducts(
        companyId: String,
        products: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val runnable = Runnable {
            exposeBoxApi.setProducts(companyId, products).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun sendCustomEvent(
        companyId: String,
        data: Map<String, Any>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val runnable = Runnable {
            exposeBoxApi.sendCustomEvent(companyId, data)
                .enqueue(object : Callback<Unit> {
                    override fun onFailure(call: Call<Unit?>, t: Throwable) {
                        afterRequestSent()
                        realmManager.saveRequest(
                            call.request().method,
                            call.request().url.toString(),
                            null
                        )
                        onFailed(t)
                    }

                    override fun onResponse(call: Call<Unit?>, response: Response<Unit>) {
                        onSuccess()
                        afterRequestSent()
                    }
                })
        }
        addToQueue(runnable)
    }

    override fun click(
        companyId: String,
        placementId: String,
        widgetId: Int,
        productId: String,
        additionalData: Map<String, Any>?,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mutableMapOf(
            RequestMapKeys.CLICK_PLACEMENT_ID.requestMapKey to placementId,
            RequestMapKeys.CLICK_WIDGET_ID.requestMapKey to widgetId,
            RequestMapKeys.CLICK_PRODUCT_ID.requestMapKey to productId
        )

        if (!additionalData.isNullOrEmpty()) {
            additionalData.forEach {
                dataMap[RequestMapKeys.CLICK_ADDITIONAL_DATA.requestMapKey + "[${it.key}]"] =
                    it.value
            }
        }

        val runnable = Runnable {
            exposeBoxApi.sendClickEvent(companyId, dataMap).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun addToWishList(
        companyId: String,
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mapOf(RequestMapKeys.ADD_TO_WISHLIST_PRODUCT_ID.requestMapKey to productId)

        val runnable = Runnable {
            exposeBoxApi.addToWishlist(companyId, dataMap).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun removeFromWishList(
        companyId: String,
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap =
            mapOf(RequestMapKeys.REMOVE_FROM_WISHLIST_PRODUCT_ID.requestMapKey to productId)

        val runnable = Runnable {
            exposeBoxApi.removeFromWishlist(companyId, dataMap)
                .enqueue(object : Callback<Unit> {
                    override fun onFailure(call: Call<Unit>, t: Throwable) {
                        afterRequestSent()
                        realmManager.saveRequest(
                            call.request().method,
                            call.request().url.toString(),
                            null
                        )
                        onFailed(t)
                    }

                    override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                        onSuccess()
                        afterRequestSent()
                    }
                })
        }

        addToQueue(runnable)
    }

    override fun realImpression(
        companyId: String,
        batches: List<RealImpressionBatch>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit,
        afterRequestSent: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val mapData = mutableMapOf<String, Any>()
        for (n in batches.indices) {
            mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.PLACEMENT_ID.requestMapKey] =
                batches[n].placementId
            mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.WIDGET_ID.requestMapKey] =
                batches[n].widgetId

            val itemsList = batches[n].items

            if (!itemsList.isNullOrEmpty()) {
                for (i in itemsList.indices) {
                    itemsList[i].forEach {
                        mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.ITEMS.requestMapKey + "[$i][${it.key}]"] =
                            it.value
                    }
                }
            }
        }

        val runnable = Runnable {
            exposeBoxApi.realImpression(companyId, mapData).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    afterRequestSent()
                    realmManager.saveRequest(
                        call.request().method,
                        call.request().url.toString(),
                        null
                    )
                    onFailed(t)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    onSuccess()
                    afterRequestSent()
                }
            })
        }

        addToQueue(runnable)
    }

    override fun sendGeneralInfo(
        companyId: String,
        userId: String,
        advertisingId: String,
        appId: String,
        onSuccess: () -> Unit
    ) {
        checkDbAndSendStoredRequests()

        val dataMap = mapOf(
            RequestMapKeys.USER_ID.requestMapKey to userId,
            RequestMapKeys.AAID.requestMapKey to advertisingId,
            RequestMapKeys.APP_ID.requestMapKey to appId
        )

        exposeBoxApi.sendGeneralInfo(companyId, dataMap).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>, t: Throwable) {
                //do nothing
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                onSuccess()
            }
        })
    }

    /**
     * Sends any GET request stored in the Database
     */

    override fun sendSavedGetRequest(
        url: String,
        afterRequestSentSuccess: () -> Unit,
        afterRequestSentFailed: () -> Unit
    ) {
        exposeBoxApi.sendSavedGetRequest(url).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>, t: Throwable) {
                afterRequestSentFailed()
            }

            override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                afterRequestSentSuccess()
            }
        })
    }

    /**
     * Sends any POST request stored in the Database
     */

    override fun sendSavedPostRequest(
        url: String,
        body: Any,
        afterRequestSentSuccess: () -> Unit,
        afterRequestSentFailed: () -> Unit
    ) {
        exposeBoxApi.sendSavedPostRequest(url, body).enqueue(
            object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    afterRequestSentFailed()
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    afterRequestSentSuccess()
                }
            })
    }

    /**
     * Check if there is something in the Database and send them in Queue
     */

    override fun checkDbAndSendStoredRequests() {
        val pendingRequestsList = realmManager.getAllRequests()
        pendingRequestsList.forEach {
            val runnable = Runnable { sendStoredRequests(it) { cleanQueue() } }
            addToQueue(runnable)
        }
    }

    /**
     * Removes stored request from the top of the Queue and start next request if Queue is not empty.
     */

    override fun cleanQueue() {
        requestQueue.remove()
        if (requestQueue.isNotEmpty()) {
            handlerThread.getHandler().post(requestQueue.first())
        }
    }

    /**
     * Function used for sending stored requests.
     */

    private fun sendStoredRequests(request: OfflineRequest, afterRequestSent: () -> Unit = {}) {
        when (request.methodType) {
            APIMethodType.GET.methodType -> sendSavedGetRequest(
                request.url,
                afterRequestSentSuccess = {
                    realmManager.delete(request.uuid)
                    afterRequestSent()
                },
                afterRequestSentFailed = {
                    afterRequestSent()
                })

            APIMethodType.POST.methodType -> convertFromBytes(request.body)?.let {
                sendSavedPostRequest(
                    request.url,
                    it,
                    afterRequestSentSuccess = {
                        realmManager.delete(request.uuid)
                        afterRequestSent()
                    },
                    afterRequestSentFailed = {
                        afterRequestSent()
                    })
            }
        }
    }

    /**
     * Adds new request to the end of request Queue and starts executing request if Queue is empty
     */

    private fun addToQueue(runnable: Runnable) {
        if (requestQueue.isEmpty()) {
            requestQueue.add(runnable)
            handlerThread.getHandler().post(requestQueue.first())
        } else {
            requestQueue.add(runnable)
        }
    }
}