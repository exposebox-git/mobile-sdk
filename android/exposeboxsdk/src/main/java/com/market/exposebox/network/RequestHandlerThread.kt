package com.market.exposebox.network

import android.os.Handler
import android.os.HandlerThread
import android.os.Process

/**
 * Used for handling network requests Queue.
 */

internal class RequestHandlerThread : HandlerThread("RequestHandlerThread", Process.THREAD_PRIORITY_BACKGROUND) {
    private lateinit var handler: Handler
    override fun onLooperPrepared() {
        handler = Handler()
    }

    fun getHandler() : Handler{
        return handler
    }
}