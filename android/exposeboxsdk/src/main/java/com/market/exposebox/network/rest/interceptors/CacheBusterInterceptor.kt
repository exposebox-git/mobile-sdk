package com.market.exposebox.network.rest.interceptors

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Used to create cache bust at the end of request.
 */

internal class CacheBusterInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val newUrl = request.url.newBuilder()
            .addQueryParameter("_", (0..100000).random().toString())
            .build()

        return request.newBuilder()
            .url(newUrl)
            .build()
            .let { chain.proceed(it) }
    }
}