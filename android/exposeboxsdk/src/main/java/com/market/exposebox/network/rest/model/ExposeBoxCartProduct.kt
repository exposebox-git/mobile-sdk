package com.market.exposebox.network.rest.model

/**
* Object for cart product representation
* @param productId: Identifier of product
* @param quantity: Quantity of products
* @param unitPrice: Price per Unit
 */

data class ExposeBoxCartProduct(
     val productId: String,
     val quantity: Int,
     val unitPrice: Double
)