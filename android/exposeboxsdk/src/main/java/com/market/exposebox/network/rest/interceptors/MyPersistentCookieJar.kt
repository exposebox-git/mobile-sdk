package com.market.exposebox.network.rest.interceptors

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.market.exposebox.USER_ID_FIELD
import okhttp3.Cookie
import okhttp3.HttpUrl

/**
 * Persistent cookie jar class. Used to handle cookie storage.
 */

internal class MyPersistentCookieJar(private val context: Context) :
    PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context)) {

    private var userId: String = ""

    /**
     * Saves [cookies] from an HTTP response to this store according to this jar's policy.
     */

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        super.saveFromResponse(url, cookies)

        cookies.forEach {
            if (it.name.contains(USER_ID_FIELD)) {
                userId = it.value
                saveUserIdToSharedPrefs(userId)
            }
        }
    }

    /**
     * Save userId from cookie to local memory of android device.
     */

    private fun saveUserIdToSharedPrefs(userId: String) {
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString(USER_ID_FIELD, userId)
        editor.apply()
    }
}