package com.market.exposebox.network.rest.model

import java.io.Serializable

/**
Recommendations request object
* @param companyId Identifier of company
* @param placementIds List of recommendations
 */

internal data class RecommendationsRequest(
    var companyId: String = "",
    var placementIds: List<String>
) : Serializable