package com.market.exposebox.network.rest.interceptors

import okhttp3.Interceptor
import okhttp3.Response

private const val USER_AGENT_TAG = "User-Agent"

/**
 * User agent interceptor is used to add "User Agent" header to all requests.
 */

internal class UserAgentInterceptor(private val userAgent: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestWithUserAgent = originalRequest.newBuilder()
            .header(USER_AGENT_TAG, userAgent)
            .build()
        return chain.proceed(requestWithUserAgent)
    }
}