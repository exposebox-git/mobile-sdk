package com.market.exposebox.network.rest.model

/**
 * Customer data object
 *  @param email: Mandatory customer email
 *  @param customerId: Mandatory customer identifier
 */

data class ExposeBoxCustomerData(
    var email: String,
    var customerId: String,
    private var firstName: String? = null,
    private var lastName: String? = null,
    private var address1: String? = null,
    private var address2: String? = null,
    private var city: String? = null,
    private var country: String? = null,
    private var state: String? = null,
    private var zipCode: String? = null,
    private var phone: String? = null,
    private var freeData: Map<String, String>? = null,
    private var freeDataPatch: Map<String, String>? = null
) {

    fun toMap(): Map<String, Any?> {

        val customerData = mutableMapOf<String, Any>(
            CustomerDataField.EMAIL.customerField to email,
            CustomerDataField.CUSTOMER_ID.customerField to customerId
        )

        firstName?.let { customerData.put(CustomerDataField.FIRST_NAME.customerField, it) }
        lastName?.let { customerData.put(CustomerDataField.LAST_NAME.customerField, it) }
        address1?.let { customerData.put(CustomerDataField.ADDRESS_1.customerField, it) }
        address2?.let { customerData.put(CustomerDataField.ADDRESS_2.customerField, it) }
        city?.let { customerData.put(CustomerDataField.CITY.customerField, it) }
        country?.let { customerData.put(CustomerDataField.COUNTRY.customerField, it) }
        state?.let { customerData.put(CustomerDataField.STATE.customerField, it) }
        zipCode?.let { customerData.put(CustomerDataField.ZIP_CODE.customerField, it) }
        phone?.let { customerData.put(CustomerDataField.PHONE.customerField, it) }
        freeData?.let { customerData.put(CustomerDataField.FREE_DATA.customerField, it) }
        freeDataPatch?.let { customerData.put(CustomerDataField.FREE_DATA_PATCH.customerField, it) }
        return customerData
    }

    enum class CustomerDataField(val customerField: String) {
        FIRST_NAME("customer[firstName]"),
        LAST_NAME("customer[lastName]"),
        EMAIL("customer[email]"),
        CUSTOMER_ID("customer[customerId]"),
        ADDRESS_1("customer[address1]"),
        ADDRESS_2("customer[address2]"),
        CITY("customer[city]"),
        COUNTRY("customer[country]"),
        STATE("customer[state]"),
        ZIP_CODE("customer[zipCode]"),
        PHONE("customer[phone]"),
        FREE_DATA("customer[freeData]"),
        FREE_DATA_PATCH("customer[freeDataPatch]")
    }
}