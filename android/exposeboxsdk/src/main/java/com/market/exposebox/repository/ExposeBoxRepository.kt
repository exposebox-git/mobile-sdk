package com.market.exposebox.repository

import com.market.exposebox.network.rest.model.*

internal interface ExposeBoxRepository {
    fun addToCart(
        companyId: String,
        productId: String,
        quantity: Int,
        unitPrice: Double,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun removeFromCart(
        companyId: String,
        productId: String,
        quantity: Int,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun getRecommendations(
        recommendationsRequest: RecommendationsRequest,
        onSuccess: ((RecommendationsResponse?) -> Unit),
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun setCategories(
        companyId: String,
        categories: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun view(
        companyId: String,
        viewName: String,
        sessionDuration: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun setTags(
        companyId: String,
        tagMap: Map<String, List<String>>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun setCustomerData(
        companyId: String,
        data: ExposeBoxCustomerData,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun conversion(
        companyId: String,
        orderId: String,
        totalPrice: Double,
        cartProducts: List<ExposeBoxCartProduct>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun setProducts(
        companyId: String,
        products: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun sendCustomEvent(
        companyId: String,
        data: Map<String, Any>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun click(
        companyId: String,
        placementId: String,
        widgetId: Int,
        productId: String,
        additionalData: Map<String, Any>?,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun addToWishList(
        companyId: String,
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun removeFromWishList(
        companyId: String,
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun realImpression(
        companyId: String,
        batches: List<RealImpressionBatch>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit = {},
        afterRequestSent: () -> Unit = {}
    )

    fun sendGeneralInfo(
        companyId: String,
        userId: String,
        advertisingId: String,
        appId: String,
        onSuccess: () -> Unit
    )

    fun sendSavedGetRequest(
        url: String,
        afterRequestSentSuccess: () -> Unit = {},
        afterRequestSentFailed: () -> Unit = {}
    )

    fun sendSavedPostRequest(
        url: String,
        body: Any,
        afterRequestSentSuccess: () -> Unit = {},
        afterRequestSentFailed: () -> Unit = {}
    )

    fun checkDbAndSendStoredRequests()

    fun cleanQueue()
}