package com.market.exposebox.db

import com.market.exposebox.convertToBytes
import com.market.exposebox.network.rest.model.OfflineRequest
import com.market.exposebox.network.rest.model.OfflineRequestRealm
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.kotlin.where
import java.util.*

private const val SCHEMA_V_NOW: Long = 2
const val UUID_KEY = "uuid"
private const val REALM_DB_NAME = "requests.realm"

internal class RealmManagerImpl : RealmManager {

    /**
     * Used for saving any unsent request to database.
     *
     * @param methodType type of API request.
     * @param url API request URL.
     * @param body API request body. When saving request with body, it is converted to bytes for
     * storage in Database.
     * @param onSuccess completion block. Executed when save operation is complete.
     *
     */

    override fun saveRequest(
        methodType: String,
        url: String,
        body: Any?,
        onSuccess: () -> Unit
    ) {
        val realm = Realm.getInstance(setup())
        realm.executeTransaction {
            val savedRequest =
                it.createObject(OfflineRequestRealm::class.java, UUID.randomUUID().toString())
            savedRequest.methodType = methodType
            savedRequest.url = url
            if (body != null) {
                savedRequest.body = convertToBytes(body)
            }
        }
        realm.close()
    }

    /**
     * Delete method that removes item from database by uuid.
     *
     *  @param uuid unique ID of the item in Database.
     *
     */

    override fun delete(uuid: String, onSuccess: () -> Unit) {
        val realm = Realm.getInstance(setup())
        val storedObject = realm.where<OfflineRequestRealm>().equalTo(UUID_KEY, uuid).findFirst()
        realm.executeTransaction {
            storedObject?.deleteFromRealm()
        }
        realm.close()
    }

    /**
     * Return all requests stored in Database
     *
     *  @param onSuccess completion block. Executed when save operation is complete.
     *
     */

    override fun getAllRequests(onSuccess: (result: OfflineRequestRealm?) -> Unit): MutableList<OfflineRequest> {
        val realm = Realm.getInstance(setup())
        val offlineRequestsList = mutableListOf<OfflineRequest>()
        val allRequests = realm.where<OfflineRequestRealm>().findAll().toMutableList()
        allRequests.forEach {
            offlineRequestsList.add(OfflineRequest(it.uuid, it.methodType, it.url, it.body))
        }
        realm.close()
        return offlineRequestsList
    }

    /**
     * Configures Realm Database.
     */

    override fun setup(): RealmConfiguration {
        return RealmConfiguration.Builder()
            .name(REALM_DB_NAME)
            .schemaVersion(SCHEMA_V_NOW)
            .deleteRealmIfMigrationNeeded()
            .build()
    }

    /**
     * Clear Database.
     */

    override fun deleteAll() {
        val realm = Realm.getInstance(setup())
        realm.executeTransaction {
            it.deleteAll()
        }
        realm.close()
    }
}