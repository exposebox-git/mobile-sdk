package com.market.exposebox.network.rest.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

/**
 * Object for sending OfflineRequest from Realm Database
 *
 * @param uuid Unique id of every stored request
 * @param methodType API method type
 * @param url Request url
 * @param body object converted to ByteArray
 *
 */

internal open class OfflineRequestRealm(
    @PrimaryKey
    var uuid: String = "",
    @Required
    var methodType: String = "",
    @Required
    var url: String = "",
    var body: ByteArray? = null
) : RealmObject()