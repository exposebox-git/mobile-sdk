package com.market.exposebox.network.rest.model

/**
Batch object to group real impression objects
* @param placementId: Identifier of placement
* @param widgetId: Identifier of widget
* @param items: key value pairs of items
 */

data class RealImpressionBatch(
    val placementId: String,
    val widgetId: Int,
    val items: List<Map<String, String>>?
)