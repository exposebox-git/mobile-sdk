package com.market.exposebox.network.rest.api

import com.google.gson.JsonObject
import com.market.exposebox.network.rest.model.RecommendationsRequest
import com.market.exposebox.network.rest.model.RecommendationsResponse
import retrofit2.Call
import retrofit2.http.*

internal interface ExposeBoxApi {

    @JvmSuppressWildcards
    @GET("px/{companyId}/event")
    fun realImpression(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, Any>
    ): Call<Unit>

    @JvmSuppressWildcards
    @GET("px/{companyId}/event")
    fun sendConversion(@Path("companyId") companyId: String, @QueryMap data: Map<String, Any>): Call<Unit>

    @GET("px/{companyId}/event")
    fun addToCart(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, String>
    ): Call<Unit>

    @GET("px/{companyId}/event")
    fun removeFromCart(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, String>
    ): Call<Unit>

    @POST("api/item-recommendations")
    fun getRecommendations(@Body body: RecommendationsRequest): Call<RecommendationsResponse>

    @GET("px/{companyId}/view")
    fun setProducts(
        @Path("companyId") companyId: String,
        @Query("products[]") data: List<String>
    ): Call<Unit>

    @GET("px/{companyId}/view")
    fun setCategories(
        @Path("companyId") companyId: String,
        @Query("categories[]") data: List<String>
    ): Call<Unit>

    @GET("px/{companyId}/view")
    fun sendGeneralInfo(
        @Path("companyId") companyId: String,
        @QueryMap generalInfo: Map<String, String>
    ): Call<Unit>

    @JvmSuppressWildcards
    @GET("px/{companyId}/event")
    fun sendCustomEvent(@Path("companyId") companyId: String, @QueryMap data: Map<String, Any>): Call<Unit>

    @GET("px/{companyId}/event")
    fun addToWishlist(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, String>
    ): Call<Unit>

    @GET("px/{companyId}/event")
    fun removeFromWishlist(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, String>
    ): Call<Unit>

    @JvmSuppressWildcards
    @GET("px/{companyId}/view")
    fun setTags(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, Any>
    ): Call<Unit>

    @GET("px/{companyId}/view")
    fun view(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, String?>
    ): Call<Unit>

    @JvmSuppressWildcards
    @GET("px/{companyId}/view")
    fun setCustomerData(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, Any?>
    ): Call<Unit>

    @JvmSuppressWildcards
    @GET("px/{companyId}/event")
    fun sendClickEvent(
        @Path("companyId") companyId: String,
        @QueryMap data: Map<String, Any>
    ): Call<Unit>

    @GET
    fun sendSavedGetRequest(@Url url:String) : Call<Unit>

    @POST
    fun sendSavedPostRequest(@Url url: String,@Body body: Any) :Call<JsonObject>

}