package com.market.exposebox.sdk

import com.market.exposebox.network.rest.model.ExposeBoxCartProduct
import com.market.exposebox.network.rest.model.ExposeBoxCustomerData
import com.market.exposebox.network.rest.model.RealImpressionBatch
import com.market.exposebox.network.rest.model.RecommendationsResponse

internal interface ExposeBoxRequestService {
    fun addToCart(
        productId: String,
        quantity: Int = 1,
        unitPrice: Double,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun removeFromCart(
        productId: String,
        quantity: Int,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun recommendations(
        placementsIds: List<String>,
        onSuccess: ((RecommendationsResponse?) -> Unit) = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun setCategories(
        categoriesNames: List<String>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun view(viewName: String, onSuccess: () -> Unit = {}, onFailed: (t: Throwable) -> Unit = {})

    fun setTags(
        tagMap: Map<String, List<String>>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun setCustomerData(
        customerData: ExposeBoxCustomerData,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun conversion(
        orderId: String,
        totalPrice: Double,
        cartProducts: List<ExposeBoxCartProduct>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun setProducts(
        productIds: List<String>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun customEvent(
        eventName: String,
        data: Map<String, Any>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun click(
        placementId: String,
        widgetId: Int,
        productId: String,
        additionalData: Map<String, String>? = null,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun addToWishlist(
        productId: String,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun removeFromWishlist(
        productId: String,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )

    fun realImpression(
        batches: List<RealImpressionBatch>,
        onSuccess: () -> Unit = {},
        onFailed: (t: Throwable) -> Unit = {}
    )
}