package com.market.exposebox.db

import com.market.exposebox.network.rest.model.OfflineRequest
import com.market.exposebox.network.rest.model.OfflineRequestRealm
import io.realm.RealmConfiguration

internal interface RealmManager {
    fun saveRequest(methodType: String, url: String, body: Any? = null, onSuccess: () -> Unit = {})
    fun delete(uuid: String, onSuccess: () -> Unit = {})
    fun getAllRequests(onSuccess: (result: OfflineRequestRealm?) -> Unit = {}): MutableList<OfflineRequest>
    fun setup(): RealmConfiguration
    fun deleteAll()
}