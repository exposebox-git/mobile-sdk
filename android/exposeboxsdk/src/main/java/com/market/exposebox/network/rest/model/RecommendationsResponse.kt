package com.market.exposebox.network.rest.model

import com.google.gson.Gson

/**
 * A representation of a placement and the recommended products for that placement
 *
 * @param userId identification number of User
 * @param placements the placement ID, e.g. `mainPagePlacement`
 *
 */

data class RecommendationsResponse(
    val userId: String?,
    val iftags: Boolean?,
    val placements: Map<String, Placement>?,
    val slu: Long?
) {

    /**
     * Used by React
     */

    fun toJson(): String {
        return Gson().toJson(this)
    }
}

data class Placement(
    val placementId: String?,
    val abt: Int?,
    val modelConfigId: Int?,
    val layoutId: Int?,
    val widgetId: Int?,
    val layoutUpdateTime: Long?,
    val products: List<Product>?
)

data class Product(
    val id: String?,
    val companyId: Int?,
    val saleOpen: Boolean?,
    val title: String?,
    val description: String?,
    val brand: String?,
    val category: String?,
    val price: Double?,
    val url: String?,
    val freeData: Map<String, String>?,
    val onSale: Boolean?,
    val categoryBreadcrumbs: List<String>,
    val imageUrl: String?
)