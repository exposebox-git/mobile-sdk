package com.market.exposebox.sdk

import android.content.Context
import android.os.AsyncTask
import android.os.Handler
import androidx.preference.PreferenceManager
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.market.exposebox.USER_ID_FIELD
import com.market.exposebox.checkDate
import com.market.exposebox.createUserAgent
import com.market.exposebox.network.rest.model.*
import com.market.exposebox.repository.ExposeBoxRepository
import com.market.exposebox.repository.ExposeBoxRepositoryImpl
import io.realm.Realm
import java.io.IOException
import java.util.concurrent.TimeUnit

private const val SESSION_DURATION_KEY = "sessionDuration"
private const val TEN_MINUTES: Long = 600

class ExposeBox(
    private val context: Context,
    private val companyId: String,
    private val appId: String = "",
    private val coppaCompliance: Boolean = true,
    private val gdprCompliance: Boolean = true
) : ExposeBoxRequestService {

    private val userAgent = context.createUserAgent()
    private var sessionStartDate: Long = System.currentTimeMillis()
    private val exposeBoxRepositoryImpl: ExposeBoxRepository?
    private var advertisingId: String = ""
    private var isSendingEnabled: Boolean = coppaCompliance && gdprCompliance
    private val handler = Handler()
    private val sendGeneralInfoRunnable = object : Runnable {
        override fun run() {
            generalInfo()
            handler.postDelayed(this, TimeUnit.SECONDS.toMillis(TEN_MINUTES))
        }
    }

    init {
        Realm.init(context)
        exposeBoxRepositoryImpl = if (isSendingEnabled) {
            ExposeBoxRepositoryImpl(userAgent, context)
        } else {
            null
        }
        sendStoredRequests()
        getAdvertisingId(context) { handler.postDelayed(sendGeneralInfoRunnable, 0) }
    }

    companion object {
        private var exposeBoxInstance: ExposeBox? = null

        fun configure(
            applicationContext: Context,
            companyId: String,
            appId: String = "",
            coppaCompliance: Boolean,
            gdprCompliance: Boolean
        ) {
            if (companyId.isEmpty()) throw Exception("Company ID cannot be empty")
            if (exposeBoxInstance == null) exposeBoxInstance =
                ExposeBox(
                    applicationContext.applicationContext,
                    companyId,
                    appId,
                    coppaCompliance,
                    gdprCompliance
                )
        }

        fun getInstance(): ExposeBox = exposeBoxInstance
            ?: throw IllegalStateException("ExposeBoxSDK hasn't been initialized. Please initialize SDK")
    }

    override fun addToCart(
        productId: String,
        quantity: Int,
        unitPrice: Double,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.addToCart(
            companyId,
            productId,
            quantity,
            unitPrice,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun removeFromCart(
        productId: String,
        quantity: Int,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.removeFromCart(
            companyId,
            productId,
            quantity,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun recommendations(
        placementsIds: List<String>,
        onSuccess: (RecommendationsResponse?) -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        val recommendationsRequest = RecommendationsRequest(companyId, placementsIds)
        exposeBoxRepositoryImpl?.getRecommendations(
            recommendationsRequest,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun setCategories(
        categoriesNames: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.setCategories(
            companyId,
            categoriesNames,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun view(
        viewName: String, onSuccess: () -> Unit, onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.view(
            companyId,
            viewName,
            sessionStartDate.toString(),
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun setTags(
        tagMap: Map<String, List<String>>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.setTags(
            companyId,
            tagMap,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun setCustomerData(
        customerData: ExposeBoxCustomerData,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        if (customerData.email.isEmpty() || customerData.customerId.isEmpty()) {
            throw IllegalStateException("Customer email and id must not be null or empty")
        }
        exposeBoxRepositoryImpl?.setCustomerData(
            companyId,
            customerData,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun conversion(
        orderId: String,
        totalPrice: Double,
        cartProducts: List<ExposeBoxCartProduct>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.conversion(
            companyId,
            orderId,
            totalPrice,
            cartProducts,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun setProducts(
        productIds: List<String>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.setProducts(
            companyId,
            productIds,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun customEvent(
        eventName: String,
        data: Map<String, Any>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        val dataMap = mutableMapOf<String, Any>()
        data.forEach {
            dataMap[eventName + "[${it.key}]"] = it.value
        }
        exposeBoxRepositoryImpl?.sendCustomEvent(
            companyId,
            dataMap,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun click(
        placementId: String,
        widgetId: Int,
        productId: String,
        additionalData: Map<String, String>?,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.click(
            companyId,
            placementId,
            widgetId,
            productId,
            additionalData,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun addToWishlist(
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.addToWishList(
            companyId,
            productId,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun removeFromWishlist(
        productId: String,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.removeFromWishList(
            companyId,
            productId,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    override fun realImpression(
        batches: List<RealImpressionBatch>,
        onSuccess: () -> Unit,
        onFailed: (t: Throwable) -> Unit
    ) {
        exposeBoxRepositoryImpl?.realImpression(
            companyId,
            batches,
            onSuccess,
            onFailed,
            afterRequestSent = {
                generalInfo()
                exposeBoxRepositoryImpl.cleanQueue()
            }
        )
    }

    /**
     * Resets time when application was started to current time.
     */

    fun resetSession() {
        sessionStartDate = System.currentTimeMillis()
    }

    /**
     * Sends general info once a day.
     */

    private fun generalInfo() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val storedDate = preferences.getLong(SESSION_DURATION_KEY, 0)
        val storedUserId: String = preferences.getString(USER_ID_FIELD, "")!!

        if (!checkDate(sessionStartDate, storedDate) && storedUserId.isNotEmpty()) {
            exposeBoxRepositoryImpl?.sendGeneralInfo(
                companyId,
                storedUserId,
                advertisingId,
                appId,
                onSuccess = {
                    val editor = preferences.edit()
                    editor.putLong(SESSION_DURATION_KEY, sessionStartDate)
                    editor.apply()
                })
        }
    }

    private fun sendStoredRequests() {
        exposeBoxRepositoryImpl?.checkDbAndSendStoredRequests()
    }

    /**
     * Retrieves advertising ID asynchronously.
     */

    private fun getAdvertisingId(context: Context, onGettingId: () -> Unit) {
        AsyncTask.execute {
            try {
                val advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)
                advertisingId = advertisingIdInfo.id
                onGettingId()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Returns GDPR Compliance boolean.
     */

    fun hasGDPRCompliance(): Boolean {
        return gdprCompliance
    }

    /**
     * Returns COPPA Compliance boolean.
     */

    fun hasCOPPACompliance(): Boolean {
        return coppaCompliance
    }
}