package com.market.exposebox.network.rest.model

/**
 * Object for storing OfflineRequest in Realm Database
 *
 * @param uuid Unique id of every stored request
 * @param methodType API method type
 * @param url Request url
 * @param body object converted to ByteArray
 *
 */

internal data class OfflineRequest(
    val uuid: String,
    val methodType: String,
    val url: String,
    var body: ByteArray? = null
)