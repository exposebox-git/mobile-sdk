package com.market.exposebox

import android.content.Context
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.text.SimpleDateFormat
import java.util.*

private const val DATE_FORMAT = "yyyyMMdd"
const val USER_ID_FIELD = "_etn"

enum class RequestMapKeys(val requestMapKey: String) {
    ADD_TO_CART_PRODUCT_ID("addToCart[productId]"),
    ADD_TO_CART_QUANTITY("addToCart[quantity]"),
    ADD_TO_CART_UNIT_PRICE("addToCart[unitPrice]"),
    REMOVE_FROM_CART_PRODUCT_ID("removeFromCart[productId]"),
    REMOVE_FROM_CART_QUANTITY("removeFromCart[quantity]"),
    VIEW_NAME("viewName"),
    SESSION_DURATION("sessionDuration"),
    CONVERSION("conversion"),
    CONVERSION_CART_PRODUCTS("conversion[cartProducts]"),
    CONVERSION_ORDER_ID("conversion[orderId]"),
    CONVERSION_TOTAL_PRICE("conversion[totalPrice]"),
    PRODUCT_ID("[productId]"),
    QUANTITY("[quantity]"),
    UNIT_PRICE("[unitPrice]"),
    CLICK_PLACEMENT_ID("click[placementId]"),
    CLICK_WIDGET_ID("click[widgetId]"),
    CLICK_PRODUCT_ID("click[productId]"),
    CLICK_ADDITIONAL_DATA("click[additionalData]"),
    ADD_TO_WISHLIST_PRODUCT_ID("addToWishlist[productId]"),
    REMOVE_FROM_WISHLIST_PRODUCT_ID("removeFromWishlist[productId]"),
    REAL_IMPRESSION("realImpression"),
    PLACEMENT_ID("[placementId]"),
    WIDGET_ID("[widgetId]"),
    ITEMS("[items]"),
    TAGS("tags"),
    APP_ID("appId"),
    AAID("aaid"),
    USER_ID("userId")
}

enum class APIMethodType(val methodType: String) {
    POST("POST"),
    GET("GET")
}

/**
 * Convert object to bytes. Used to store objects in DB
 */

fun convertToBytes(`object`: Any): ByteArray? {
    ByteArrayOutputStream().use { bos ->
        ObjectOutputStream(bos).use { out ->
            out.writeObject(`object`)
            return bos.toByteArray()
        }
    }
}

/**
 * Convert object from bytes. Used when pulling info from Database.
 */

fun convertFromBytes(bytes: ByteArray?): Any? {
    ByteArrayInputStream(bytes).use { bis ->
        ObjectInputStream(bis).use { `in` -> return `in`.readObject() } }
}

/**
 * Compares two dates. Used for determining if General User Info has been sent today.
 */

fun checkDate(todayDate: Long, storedDate: Long): Boolean {
    val fmt = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
    return fmt.format(todayDate) == fmt.format(storedDate);
}

/**
 * Function for creation of User Agent string.
 */

fun Context.createUserAgent(): String {
    val deviceInfo = System.getProperty("http.agent")
    val stringId = applicationInfo.labelRes

    val appName = if (stringId == 0) {
        applicationInfo.nonLocalizedLabel.toString()
    } else {
        this.getString(stringId)
    }
    val appVersionNumber = packageManager.getPackageInfo(packageName, 0).versionName.toString()

    return "$appName/$appVersionNumber $deviceInfo"
}