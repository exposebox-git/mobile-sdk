package com.market.exposebox.repository

import com.market.exposebox.RequestMapKeys
import com.market.exposebox.network.rest.api.ExposeBoxApi
import com.market.exposebox.network.rest.model.*
import com.market.exposeboxsdk.BuildConfig
import com.google.gson.GsonBuilder
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val TEST_COMPANY_ID = "30510"

class ExposeBoxRepositoryImplTest {
    private lateinit var exposeBoxApi: ExposeBoxApi

    @Before
    fun setup() {

        exposeBoxApi = Retrofit.Builder()
            .baseUrl(BuildConfig.EXPOSE_BOX_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(ExposeBoxApi::class.java)

    }

    @Test
    fun addToCart() {

        val productId = "product-1"
        val quantity = 1
        val unitPrice = 99.0

        val mapData = mutableMapOf(
            RequestMapKeys.ADD_TO_CART_PRODUCT_ID.requestMapKey to productId,
            RequestMapKeys.ADD_TO_CART_QUANTITY.requestMapKey to quantity.toString(),
            RequestMapKeys.ADD_TO_CART_UNIT_PRICE.requestMapKey to unitPrice.toString()
        )

        val product = exposeBoxApi.addToCart(TEST_COMPANY_ID, mapData).execute()

        assert(product.code() == 200)
    }

    @Test
    fun removeFromCart() {

        val productId = "product-1"
        val quantity = 1

        val mapData = mutableMapOf(
            RequestMapKeys.REMOVE_FROM_CART_PRODUCT_ID.requestMapKey to productId,
            RequestMapKeys.REMOVE_FROM_CART_QUANTITY.requestMapKey to quantity.toString()
        )

        val product = exposeBoxApi.removeFromCart(TEST_COMPANY_ID, mapData).execute()

        assert(product.code() == 200)
    }

    @Test
    fun getRecommendations() {
        val product = exposeBoxApi.getRecommendations(
            RecommendationsRequest(
                TEST_COMPANY_ID,
                listOf("mainPagePlacement")
            )
        ).execute().body() as RecommendationsResponse

        assert(product.slu != null)
    }

    @Test
    fun setCategories() {
        val product = exposeBoxApi.setCategories(
            TEST_COMPANY_ID,
            listOf("product-1", "product-2", "product-3")
        ).execute()

        assert(product.code() == 200)
    }

    @Test
    fun view() {
        val dataMap = mapOf(
            RequestMapKeys.VIEW_NAME.requestMapKey to "MainActivity",
            RequestMapKeys.SESSION_DURATION.requestMapKey to System.currentTimeMillis().toString()
        )

        val product = exposeBoxApi.view(TEST_COMPANY_ID, dataMap).execute()

        assert(product.code() == 200)
    }

    @Test
    fun setTags() {
        val dataMap = mutableMapOf<String, Any>()
        val tagMap = mapOf("someTag" to listOf("Tag1", "Tag2"))

        tagMap.forEach {
            for (n in it.value.indices) {
                dataMap[RequestMapKeys.TAGS.requestMapKey + "[${it.key}][$n]"] = it.value[n]
            }
        }

        val product = exposeBoxApi.setTags(TEST_COMPANY_ID, tagMap).execute()
        assert(product.code() == 200)
    }

    @Test
    fun setCustomerData() {
        val customerData = ExposeBoxCustomerData(
            "test@gmail.com",
            "2014",
            "firstName",
            "lastName",
            "addr1",
            "addr2",
            "city",
            "county",
            "state",
            "zipCode",
            "phone",
            mapOf("freedata" to "freedata"),
            mapOf("freedatapatch" to "freedatapatch")
        )
        val product = exposeBoxApi.setCustomerData(TEST_COMPANY_ID, customerData.toMap()).execute()
        assert(product.code() == 200)
    }

    @Test
    fun conversion() {
        val listProducts = listOf(
            ExposeBoxCartProduct("product-1", 2, 10.0),
            ExposeBoxCartProduct("product-2", 1, 11.0)
        )
        val dataMap = mutableMapOf(
            RequestMapKeys.CONVERSION_ORDER_ID.requestMapKey to "99",
            RequestMapKeys.CONVERSION_TOTAL_PRICE.requestMapKey to 20.0
        )

        for (n in listProducts.indices) {
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.PRODUCT_ID.requestMapKey] =
                listProducts[n].productId
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.QUANTITY.requestMapKey] =
                listProducts[n].quantity
            dataMap[RequestMapKeys.CONVERSION_CART_PRODUCTS.requestMapKey + "[$n]" + RequestMapKeys.UNIT_PRICE.requestMapKey] =
                listProducts[n].unitPrice
        }

        val product = exposeBoxApi.sendConversion(TEST_COMPANY_ID, dataMap).execute()

        assert(product.code() == 200)
    }

    @Test
    fun setProducts() {
        val product =
            exposeBoxApi.setProducts(TEST_COMPANY_ID, listOf("product-1", "product-2", "product-3"))
                .execute()

        assert(product.code() == 200)
    }

    @Test
    fun sendCustomEvent() {
        val eventName = "Custom event"
        val data = mapOf("testKey" to "TestValue")
        val dataMap = mutableMapOf<String, Any>()

        data.forEach {
            dataMap[eventName + "[${it.key}]"] = it.value
        }
        val product = exposeBoxApi.sendCustomEvent(TEST_COMPANY_ID, dataMap).execute()
        assert(product.code() == 200)
    }

    @Test
    fun click() {
        val placementId = "PageView"
        val widgetId = 2180
        val productId = "product-7"
        val additionalData = mapOf("bsrId" to "additionalData")

        val dataMap = mutableMapOf(
            RequestMapKeys.CLICK_PLACEMENT_ID.requestMapKey to placementId,
            RequestMapKeys.CLICK_WIDGET_ID.requestMapKey to widgetId,
            RequestMapKeys.CLICK_PRODUCT_ID.requestMapKey to productId
        )

        if (!additionalData.isNullOrEmpty()) {
            additionalData.forEach {
                dataMap[RequestMapKeys.CLICK_ADDITIONAL_DATA.requestMapKey + "[${it.key}]"] =
                    it.value
            }
        }

        val product = exposeBoxApi.sendClickEvent(TEST_COMPANY_ID, dataMap).execute()
        assert(product.code() == 200)
    }

    @Test
    fun addToWishList() {
        val productId = "product-1"
        val dataMap = mapOf(RequestMapKeys.ADD_TO_WISHLIST_PRODUCT_ID.requestMapKey to productId)

        val product = exposeBoxApi.addToWishlist(TEST_COMPANY_ID, dataMap).execute()
        assert(product.code() == 200)
    }

    @Test
    fun removeFromWishList() {
        val productId = "product-1"
        val dataMap =
            mapOf(RequestMapKeys.REMOVE_FROM_WISHLIST_PRODUCT_ID.requestMapKey to productId)

        val product = exposeBoxApi.removeFromWishlist(TEST_COMPANY_ID, dataMap).execute()
        assert(product.code() == 200)
    }

    @Test
    fun realImpression() {
        val mapData = mutableMapOf<String, Any>()
        val batches = listOf(
            RealImpressionBatch(
                "MyRequestPlacement", 2180, listOf(
                    mapOf(
                        "id" to "gtest12",
                        "itemType" to "product",
                        "catalogId" to "default",
                        "category" to "leonardo-negev-beer-sheba",
                        "brand" to "Leonardo"
                    )
                )
            ),
            RealImpressionBatch(
                "mainPagePlacement", 2180, listOf(
                    mapOf(
                        "id" to "gtest12",
                        "itemType" to "product",
                        "catalogId" to "default",
                        "category" to "leonardo-negev-beer-sheba",
                        "brand" to "Leonardo"
                    )
                )
            )
        )

        for (n in batches.indices) {
            mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.PLACEMENT_ID.requestMapKey] =
                batches[n].placementId
            mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.WIDGET_ID.requestMapKey] =
                batches[n].widgetId

            val itemsList = batches[n].items
            if (!itemsList.isNullOrEmpty()) {
                for (i in itemsList.indices) {
                    itemsList[i].forEach {
                        mapData[RequestMapKeys.REAL_IMPRESSION.requestMapKey + "[$n]" + RequestMapKeys.ITEMS.requestMapKey + "[$i][${it.key}]"] =
                            it.value
                    }
                }
            }
        }

        val product = exposeBoxApi.realImpression(TEST_COMPANY_ID, mapData).execute()
        assert(product.code() == 200)
    }
}