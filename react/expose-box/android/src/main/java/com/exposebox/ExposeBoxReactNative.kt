package com.exposebox

import com.market.exposebox.sdk.ExposeBox
import com.facebook.react.bridge.ReadableArray
import com.facebook.react.bridge.ReadableMap
import android.widget.Toast
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.WritableMap
import com.facebook.react.bridge.WritableArray
import com.facebook.react.bridge.Arguments

import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.UiThreadUtil
import com.market.exposebox.network.rest.model.*
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ExposeBoxModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {

    override fun getName(): String {
        return "ExposeBoxSDK"
    }

    //handle error in callback 
    private  fun handleError():(Throwable) -> Unit = {
        Toast.makeText(reactApplicationContext, "error", 3000).show()
    }

    @ReactMethod
    fun configure(companyId: String, appId: String, hasCOPPACompliance: Boolean, hasGDPRCompliance: Boolean ) {
        // companyId: indentifier of company 
        // appId: indentifier of application
        // hasGDPRCompliance: if has or not gdpr compliance, 
        // hasCOPPACompliance:  if has or not coppa compliance
        ExposeBox.configure(reactApplicationContext, companyId, appId, hasCOPPACompliance, hasGDPRCompliance )
    }

    @ReactMethod
    fun addToCart( productId: String, quantity: Int, unitPrice: Double, handle: Callback) {
        // productId: indentifier of product, 
        // quantity: quantity of product,
        // unitPrice: price of product, 
        // handle: callback to get result of api call

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "addToCart")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "addToCart")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().addToCart(productId, quantity, unitPrice, handleResult(), handleError())
    }

    @ReactMethod
    fun removeFromCart( productId: String, quantity: Int, handle: Callback) { 
        // productId: indentifier of product, 
        // quantity: quantity of product,
        // handle: callback to get result of api call

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "removeFromCart")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "removeFromCart")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().removeFromCart(productId, quantity, handleResult(), handleError()) 
    }

    @ReactMethod
    fun setCategories(categoriesNames: ReadableArray, handle: Callback) {
        // categoriesNames - array of categories's name
        // handle: callback to get result of api call

        val partnersArray = categoriesNames.toArrayList()
        // create List to put data received from RN
        var categories = MutableList<String>(partnersArray.size){ "" }
        partnersArray.forEachIndexed { _, element ->
            categories.add(element.toString())
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "setCategories")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "setCategories")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().setCategories(categories, handleResult(), handleError())
    }

    @ReactMethod
    fun recommendations(placementsIds: ReadableArray, handle: Callback) {
        // placementsIds - array of categories's name
        // handle: callback to get result of api call

        // create Array to put data received from RN
        val placementsIdsArray = placementsIds.toArrayList()
        var ids = MutableList<String>(placementsIdsArray.size){ "" }
        placementsIdsArray.forEachIndexed { _, element ->
            ids.add(element.toString())
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "recommendations")
        fun handleResult():(RecommendationsResponse?) -> Unit = {
            success.putString("response", it.toString())
            success.putString("responseData", it?.toJson().toString())
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "setCategories")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().recommendations(ids, handleResult(), handleError())
    }
    
    @ReactMethod
    fun setTags(tagMap: ReadableMap, handle: Callback) {
        // tagMap: Map of tags
        // handle: callback to get result of api call

        // create Map to put data received from RN
        val tags = tagMap.toHashMap()
        var datas = mutableMapOf<String, MutableList<String>>()
        tags.forEach{element ->
            datas.put(element.key.toString(), element.value as MutableList<String>)
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "setTags")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "setTags")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().setTags(datas, handleResult(), handleError())
    }

    @ReactMethod
    fun setCustomerData(customerData: ReadableMap, handle: Callback) {
        // customerData: Map of customer's data
        // handle: callback to get result of api call

        // create Map to put data received from RN
        val data = customerData.toHashMap()
        val dataMapFreeData: Map<String, String> = data.get("freeData") as Map<String, String>

        // create ExposeBoxCustomerData with data received from RN
        val customerDataResult = ExposeBoxCustomerData(data.get("email").toString(), 
                                          data.get("customerId").toString(), 
                                          data.get("firstName")?.toString(),
                                          data.get("lastName")?.toString(),
                                          data.get("address1")?.toString(),
                                          data.get("address2")?.toString(),
                                          data.get("city")?.toString(),
                                          data.get("country")?.toString(),
                                          data.get("state")?.toString(),
                                          data.get("zipCode")?.toString(),
                                          data.get("phone")?.toString(),
                                          dataMapFreeData,
                                          dataMapFreeData
                                          )
        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "setCustomerData")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "setCustomerData")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }                                 
        ExposeBox.getInstance().setCustomerData(customerDataResult, handleResult(), handleError())
    }

    @ReactMethod
    fun view(viewName: String, handle: Callback) {
        // viewName: name of view
        // handle: callback to get result of api call

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "view")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "view")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        } 
        ExposeBox.getInstance().view(viewName, handleResult(), handleError())
    }

    @ReactMethod
    fun conversion(orderId: String, totalPrice: Double, cartProducts: ReadableArray, handle: Callback) {
        // orderId: indentifier of order
        // totalPrice: total price of order
        // cartProducts: products in the cart
        // handle: callback to get result of api call

        // create Array to put data received from RN
        val cartProductsArray = cartProducts.toArrayList()
        var ids = mutableListOf<ExposeBoxCartProduct>()
        cartProductsArray.forEachIndexed { _, element ->
            ids.add(ExposeBoxCartProduct(
                (element as Map<String, Any>).get("productId").toString(),
                (element as Map<String, Any>).get("quantity").toString().toDouble().toInt(), 
                (element as Map<String, Any>).get("unitPrice").toString().toDouble()
                ))
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "conversion")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "conversion")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().conversion(orderId, totalPrice, ids, handleResult(), handleError())
    }

    @ReactMethod
    fun realImpression(batches: ReadableArray, handle: Callback) {
        // batches: Array of batches
        // handle: callback to get result of api call

        // create Array to put data received from RN
        val batchesArray = batches.toArrayList()
        var result = mutableListOf<RealImpressionBatch>()
        batchesArray.forEachIndexed { _, element ->
            result.add(RealImpressionBatch(
                (element as Map<String, Any>).get("placementId").toString(),
                (element as Map<String, Any>).get("widgetId").toString().toDouble().toInt(), 
                (element as Map<String, Any>).get("items") as List<Map<String,String>>?
                ))
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "realImpression")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "realImpression")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().realImpression(result, handleResult(), handleError())
    }

    @ReactMethod
    fun setProducts(productIds: ReadableArray, handle: Callback) {
        // productId: array of indentifiers of products, 
        // handle: callback to get result of api call

        // create Array to put data received from RN
        val productIdsArray = productIds.toArrayList()
        var ids = MutableList<String>(productIdsArray.size){ "" }
        productIdsArray.forEachIndexed { _, element ->
            ids.add(element.toString())
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "setProducts")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "setProducts")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().setProducts(ids, handleResult(), handleError())
    }

    @ReactMethod
    fun customEvent( eventName: String, data: ReadableMap, handle: Callback) {
        // eventName: event's name
        // data: Map of data
        // handle: callback to get result of api call

        // create Map to put data received from RN
        val dataMap = data.toHashMap()
        var datas = mutableMapOf<String, Any>()
        dataMap?.forEach{element ->
            datas.put(element.key.toString(), element.value)
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "customEvent")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "customEvent")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().customEvent(eventName, datas, handleResult(), handleError())
    }

    @ReactMethod
    fun click( placementId: String, widgetId: Int, productId: String, additionalData: ReadableMap?, handle: Callback) {
        // placementId: indentifier of placement
        // widgetId: indentifier of pwidget 
        // productId: indentifier of product
        // additionalData: Map of additional data, not required 
        // handle: callback to get result of api call

        // create Map to put data received from RN
        val additional = additionalData?.toHashMap()
        var datas = mutableMapOf<String, String>()
        additional?.forEach{element ->
            datas.put(element.key.toString(), element.value.toString())
        }

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "click")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "click")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().click(placementId, widgetId, productId, datas, handleResult(), handleError())
    }

    @ReactMethod
    fun addToWishlist( productId: String, handle: Callback) {
        // productId: indentifier of product
        // handle: callback to get result of api call

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "addToWishlist")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "addToWishlist")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().addToWishlist(productId, handleResult(), handleError())
    }

    @ReactMethod
    fun removeFromWishlist( productId: String, handle: Callback) {
        // productId: indentifier of product
        // handle: callback to get result of api call

        // create Map, if success result - return in RN
        val success: WritableMap = Arguments.createMap()
        success.putBoolean("success", true)
        success.putString("method", "removeFromWishlist")
        fun handleResult():() -> Unit = {
            handle.invoke(null, success)
        }

        // create Map, if error result - return in RN
        val error: WritableMap = Arguments.createMap()
        error.putBoolean("success", false)
        error.putString("method", "removeFromWishlist")
        fun handleError():(Throwable) -> Unit = {
            handle.invoke(error, null)
        }
        ExposeBox.getInstance().removeFromWishlist(productId, handleResult(), handleError())
    }

    @ReactMethod
    fun resetSession() {
        ExposeBox.getInstance().resetSession()
    }
}