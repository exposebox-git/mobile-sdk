
# react-native-expose-box

## Getting started

```swift
npm install react-native-expose-box --save
```


### Mostly automatic installation

```swift
react-native link react-native-expose-box
```

### iOS 

#### Installation with [CocoaPods](https://cocoapods.org/)

Add the following to your Podfile and run pod update:

```ruby
pod 'RealmSwift', '~> 3.17', :modular_headers => true

```
```ruby
pod 'Realm', '~> 3.17', :modular_headers => true

```

### Android

#### Manual installation

##### RN >0.6

- Check if have in `android/build.gradle`: 
```gradle
maven { url 'https://jitpack.io' }
```
- Append the following lines to `android/settings.gradle`:
``` gradle
include ':react-native-expose-box' project(':react-native-expose-box').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-expose-box/android')

```
- Insert the following lines inside the dependencies block in `android/app/build.gradle`: 
``` gradle
dependencies {
	...
	compile project(':react-native-expose-box')
	...
}
```


##### RN <0.6

- Check if have in `android/build.gradle`: 
```gradle
maven { url 'https://jitpack.io' }
```
- Open up `android/app/src/main/java/[...]/MainAplication.java`, add to the imports at the top of the file
```gradle
import com.exposebox.ExposeBoxPackage;
```
- Add to the list returned by the `getPackages()` method
```gradle
new ExposeBoxPackage()
```
- Append the following lines to `android/settings.gradle`:
```gradle
   include ':react-native-expose-box'
   project(':react-native-expose-box').projectDir = new File(rootProject.projectDir,  '../node_modules/react-native-expose-box/android')
```  
- Insert the following lines inside the dependencies block in `android/app/build.gradle`: 
``` gradle
dependencies {
	...
	compile project(':react-native-expose-box')
	...
}
```

### Usage
```
import RNExposeBox from 'react-native-expose-box';
```
Configure with your `companyId (required)`, your `appId (optional)` , `COOPPA, GDPR` 
All data sent before setting companyId will be ignored.

The APP won't send the data if the GDPR or COOPPA values are set to false.
```
ExposeBox.shared.configure(companyId: "COMPANY_ID", appId: "APP_ID", coppaCompliance: true, GDPR: true)
```
`companyId` is your company's ID, `appId` is your app's ID, both provided by ExposeBox.

Then you can use ExposeBox across your app.

### Methods
Methods use callback as last parameter to catch success/fail result of API call
example:
```
handleSuccess = (error, data) => {
    if (error) {
      // handle error
    }
    if (data) {
      if (data.method === 'recommendations') {
        //handle data.response
      }
      // handle data - for success case
    }
  };
```
### Track a page view
```
ExposeBox.view(view, handleSuccess)
```
example: 
```
ExposeBox.view(''mainView', handleSuccess)
```
view is your view's name

### Set Categories
Integrating category pages enables ExposeBox to recommend products from the same category.
```
ExposeBoxSDK.setCategories(["categoryName", "categoryName"], handleSuccess)
```
example: 
```
ExposeBoxSDK.setCategories(["testCategory1", "testCategory2"], handleSuccess)
```
### Set Products
Integrating products enables ExposeBox to suggest recommended products and promote sponsored products to users.
```
ExposeBox.setProducts(['productId', 'productId'], handleSuccess)
```
example:
```
ExposeBox.setProducts(['product-7', 'product8'], handleSuccess)
```
In case of a single product, you can provide an array with a single element

### Set Tags
Adding segmentation tags enables you to tag any user visiting any page on your site into various customized segments from which you can later assemble audiences.
The Audiences will be used for running campaigns for sponsored products.
A segmentation tag contains a key-value pair which you set according to your needs.
```
ExposeBoxSDK.setTags({'key': ["tagName", 'tagName']}, handleSuccess)
```
example: 
```
ExposeBoxSDK.setTags({'key': ["tagTest", 'tagTest2']}, handleSuccess)
```

### Add and Remove from Cart
Integrating the cart enables ExposeBox to detect added products and abandoned products, and to run an API request whenever a client adds a product to their cart.

Use this when your customer is adding or removing items from the cart, where `productId`  is your product's SKU, `quantity`  is the quantity of the same product added, and `price`  the product's unit price
```
ExposeBoxSDK.addToCart(productId, quantity, price, handleSuccess)
```
example: 
```
ExposeBoxSDK.addToCart(product-8, 10, 125, handleSuccess)
```
```
ExposeBoxSDK.removeFromCart(productId, quantity, handleSuccess)
```
example: 
```
ExposeBoxSDK.removeFromCart(product-8, 5, handleSuccess)
```
### Add and Remove from Wishlist

Integrating the wishList enables ExposeBox to detect added products and abandoned products, and to run an API request whenever a client adds a product to their wishList.
```
ExposeBoxSDK.addToWishlist(productId, handleSuccess)
```
example: 
```
ExposeBoxSDK.addToWishlist(product-8, handleSuccess)
```
```
ExposeBoxSDK.removeFromWishlist(productId, handleSuccess)
```
example: 
```
ExposeBoxSDK.removeFromWishlist(product-7, handleSuccess)
```
### Customer Data
Setting customer details enables ExposeBox send automated emails and synchronise offline data with online data.

Use this when the customer is logged in and the data is available.
```
ExposeBoxSDK.setCustomerData({"email":"test@gmail.com","customerId":"id"}, handleSuccess)
```
Other fields like city, zipCode, address1, address2, phone, firstName, lastName, country, state are available via additional info.
```
ExposeBoxSDK.setCustomerData({"email":"test@gmail.com","customerId":"id", "firstName":"test", "lastName":"testName"}, handleSuccess)
```
Other additional information can be add in freeData
```
ExposeBoxSDK.setCustomerData({"email":"test@gmail.com","customerId":"id", "firstName":"test", "lastName":"testName"}, 'freeData': {'hobby':'hobby'}}, handleSuccess)
```
Then send it through ExposeBox

example: 
```
ExposeBoxSDK.setCustomerData({"email":"test@gmail.com","customerId":"343", "firstName":"Alex", "lastName":"Ivanov"}, 'freeData': {'hobby':'skying'}}, handleSuccess)
```
### Custom Event
Integrating the custom events enables ExposeBox additional knowledge of the customer's actions, e.g. Wish List, and improve the results provided by the algorithm.
```
ExposeBoxSDK.customEvent("eventName", {"data": "data"}, handleSuccess)
```
Where `eventName` is the event name that will appear on your dashboard, and `data` is your custom event data.
example: 
```
ExposeBoxSDK.customEvent("searchAndFilter", {"query": "roller-skate"}, handleSuccess)
```
### Click
Send click event for a clicked recommended product
Sponsored recommendation items contain bsrId field which should be sent as additionalData = {bsrId: recommendedItem.bsrId}
additionalData(optional)
If you don't pass data in optional field, set it to null
```
ExposeBoxSDK.click(view, widgetId, productId, {'bsrId': 'data'}, handleSuccess)
```
example: 
```
ExposeBoxSDK.click("mainPagePlacement", 21816, "product-8", {'bsrId': 'ffff'}, handleSuccess)
```
### Real Impressions
The event occurs when a customer view items that are shown on the screen, in the phone’s view port.

This event sends a batch of real impressions event 3 seconds
```
ExposeBox.realImpression([{ placementId: "ID",

       widgetId: Number,

       items: [{"id": "gtest12",

         "itemType": "product",

         "catalogId": "default",

         "category": "leonardo-negev-beer-sheba",

         "brand": "Leonardo"}]

      }])
```
example:
```
ExposeBoxSDK.realImpression([
                              {"placementId":"mainPagePlacement", "widgetId":2180,
                                "items": [{"id": "gtest12", "itemType": "product", "catalogId": "default", "category": "leonardo-negev-beer-sheba", "brand": "Leonardo"}, {"id": "gtest12", "itemType": "product", "catalogId": "default", "category": "leonardo-negev-beer-sheba", "brand": "Leonardo"}] },
                              {"placementId":"mainPagePlacement", "widgetId":2180,
                               "items": []},
                              {"placementId":"mainPagePlacement", "widgetId":2180, "items": []}], 
                              handleSuccess)
```
### Order (Conversions)
Notifies ExposeBox that a conversion was made. This should be sent when a conversion event has occurred (e.g. a “thank you” page after checkout)

First, create the cart products that were in the order, then send it through ExposeBox with orderId and totalPrice (optional) (orderId should be your order ID, which you usually get after an order was placed).
```
ExposeBox.conversion(orderId, totalPrice, cartProducts)
```
example:
```
ExposeBoxSDK.conversion("3377777", 600.45, [{productId: 'product-8', quantity: 2, unitPrice: 200.02},{productId: 'product-7', quantity: 2, unitPrice: 200.02}], handleSuccess)
```
### Get Recommendations
After you set up your placements in the ExposeBox dashboard, you can get product recommendations for those placements.

The response will contain a List of ExposeBox Placements
```
ExposeBoxSDK.recommendations(companyId, [placementIds], handleRecommendation)
```
example:
```
ExposeBoxSDK.recommendations("30510", ["mainPagePlacement"], handleSuccess)
```
### Reset Session
Use this to reset session start time to current
```
ExposeBox.resetSession()
```
### Offline Mode
In offline mode, events will be saved to the database and sent when the Internet appears and the first request is processed
