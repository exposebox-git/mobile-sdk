
Pod::Spec.new do |s|
  s.name         = "react-native-expose-box"
  s.version      = "1.0.0"
  s.summary      = "RNExposeBox"
  s.description  = <<-DESC
                  RNExposeBox
                   DESC
  s.homepage     = "https://github.com/"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "11.0"
  s.source       = { :git => "https://github.com/author/RNExposeBox.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m,swift}"
  s.requires_arc = true


  s.dependency "React"
  s.dependency 'Alamofire', '~> 5.1'
  s.dependency 'RealmSwift', '~> 3.17'
  s.dependency 'ExposeBoxPlus' 
  #s.dependency "others"

end

  