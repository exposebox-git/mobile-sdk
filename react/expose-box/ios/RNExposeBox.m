
#import <Foundation/Foundation.h>

#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(ExposeBoxSDK, NSObject)

RCT_EXTERN_METHOD(configure: (nonnull NSString *)companyId appId: ( NSString *)appId coppaCompliance:(nonnull BOOL *)coppaCompliance gdpr: (nonnull BOOL *)gdpr)

RCT_EXTERN_METHOD(click: (nonnull NSString *)placementId widgetId:(nonnull NSInteger *)widgetId productId:(nonnull NSString *)productId additionalData:(NSDictionary *)additionalData callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(view: (nonnull NSString *)viewName callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(setProducts: (nonnull NSArray *)productIds callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(setCategories: (nonnull NSArray *)categoriesNames callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(customEvent: (nonnull NSString *)name data:(nonnull NSDictionary *)data callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(addToWishlist: (nonnull NSString *)productId callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(removeFromWishlist: (nonnull NSString *)productId callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(addToCart: (nonnull NSString *)productId quantity:(nonnull NSInteger *)quantity unitPrice:(nonnull double)unitPrice callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(removeFromCart: (nonnull NSString *)productId quantity:(nonnull NSInteger *)quantity callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(conversion: (nonnull NSString *)orderId totalPrice:(nonnull double)totalPrice cartProducts:(nonnull NSArray *)cartProducts callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(setTags: (nonnull NSDictionary *)tags callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(setCustomerData: (nonnull NSDictionary *)customerData callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(recommendations: (nonnull NSArray *)placementIds callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(realImpression: (nonnull NSArray *)batches callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(resetSession)

RCT_EXTERN_METHOD(constantsToExport: [AnyHashable : Any]!)

@end

