
import Foundation
import ExposeBoxPlus

@objc(ExposeBoxSDK)
class ExposeBoxSDK: NSObject {
    private let exposeBox = ExposeBox.shared
    
    
    @objc
    static func requiresMainQueueSetup() -> Bool {
        return true
    }
    
    @objc
    func configure(_ companyId: String, appId: String, coppaCompliance: Bool, gdpr: Bool) -> Void {
        // companyId: indentifier of company 
        // appId: indentifier of application
        // gdpr: if has or not gdpr compliance, 
        // coppaCompliance:  if has or not coppa compliance
        exposeBox.configure(companyId: companyId, appId: appId, coppaCompliance: coppaCompliance, GDPR: gdpr)
    }
       
    @objc
    func click(_ placementId: String, widgetId: Int, productId: String, additionalData: [String: String]?, callback: @escaping RCTResponseSenderBlock) -> Void {
        // placementId: indentifier of placement, 
        // widgetId: indentifier of widget, 
        // productId: indentifier of product, 
        // additionalData: not reqiured field of additional data, 
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "click"] as [String: Any]
        exposeBox.click(placementId: placementId, widgetId: widgetId, productId: productId, additionalData: additionalData,
                        onSuccess: {
                            resultsDict.updateValue(true, forKey: "success")
                            callback([NSNull(), resultsDict])},
                        onFailed: { error in
                            resultsDict.updateValue(false, forKey: "success")
                            callback([error, resultsDict])
        })
    }
    
    @objc
    func view(_ viewName: String, callback: @escaping RCTResponseSenderBlock) -> Void {
        // viewName: name of view, 
        // callback: callback to get result of api call

         // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "view"] as [String: Any]
        exposeBox.view(viewName: viewName, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(),resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func setProducts(_ productIds: [String], callback: @escaping RCTResponseSenderBlock) -> Void {
        // productId: indentifiers of products, 
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "setProducts"] as [String: Any]
        exposeBox.setProducts(productIds: productIds, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func setCategories(_ categoriesNames: [String], callback: @escaping RCTResponseSenderBlock) -> Void {
        // categoriesNames - categories's name

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "setCategories"] as [String: Any]
        exposeBox.setCategories(categoriesNames: categoriesNames, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func customEvent(_ name: String, data: [String: Any], callback: @escaping RCTResponseSenderBlock) -> Void {
        // name: event's name
        // data: dictionary of data
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "customEvent"] as [String: Any]
        exposeBox.customEvent(name: name, data: data, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func addToWishlist(_ productId: String, callback: @escaping RCTResponseSenderBlock) -> Void {
        // productId: indentifiers of products, 
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "addToWishlist"] as [String: Any]
        exposeBox.addToWishlist(productId: productId, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func removeFromWishlist(_ productId: String, callback: @escaping RCTResponseSenderBlock) -> Void {
        // productId: indentifiers of products, 
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "removeFromWishlist"] as [String: Any]
        exposeBox.removeFromWishlist(productId: productId, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func addToCart(_ productId: String, quantity: Int, unitPrice: Double, callback: @escaping RCTResponseSenderBlock) -> Void {
        // productId: indentifiers of products, 
        // quantity: quantity of product,
        // unitPrice: price of product,
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "addToCart"] as [String: Any]
        exposeBox.addToCart(productId: productId, quantity: quantity, unitPrice: unitPrice, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func removeFromCart(_ productId: String, quantity: Int, callback: @escaping RCTResponseSenderBlock) -> Void {
        // productId: indentifiers of products, 
        // quantity: quantity of product,
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "removeFromCart"] as [String: Any]
        exposeBox.removeFromCart(productId: productId, quantity: quantity, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func conversion(_ orderId: String, totalPrice: Double, cartProducts: [Dictionary<String, Any>], callback: @escaping RCTResponseSenderBlock) -> Void {
        // orderId: indentifier of order
        // totalPrice: total price of order
        // cartProducts: products in the cart
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "conversion"] as [String: Any]
        let mappedCartProducts = cartProducts.map { (product) in
            ExposeBoxCartProduct(productId: product["productId"] as! String,
                                 quantity: (product["quantity"] as! NSNumber).intValue,
                                 unitPrice: (product["unitPrice"] as! NSNumber).doubleValue)
        }
        exposeBox.conversion(orderId: orderId, totalPrice: totalPrice, cartProducts: mappedCartProducts, onSuccess:  {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func setTags(_ tags: [String: [String]], callback: @escaping RCTResponseSenderBlock) -> Void {
        // tags: dictionary of tags
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "setTags"] as [String: Any]
        exposeBox.setTags(tags: tags, onSuccess: {
            resultsDict.updateValue(true, forKey: "success")
            callback([NSNull(), resultsDict])
        }, onFailed: { error in
            resultsDict.updateValue(false, forKey: "success")
            callback([error, resultsDict])
        })
    }
    
    @objc
    func setCustomerData(_ customerData: NSDictionary, callback: @escaping RCTResponseSenderBlock) -> Void {
        // customerData: dictionary of customer's data
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "setCustomerData"] as [String: Any]
        guard let customerDataDictionary = customerData as? [String: Any] else {
            return
        }
        guard let email = customerDataDictionary["email"] as? String else { return }
        guard let customerId = customerDataDictionary["customerId"] as? String else { return }

        // create ExposeBoxCustomerData with data received from RN
        let exposeBoxCustomerData = ExposeBoxCustomerData(email: email, customerId: customerId)
        exposeBoxCustomerData.firstName = customerDataDictionary["firstName"] as? String
        exposeBoxCustomerData.lastName = customerDataDictionary["lastName"] as? String
        exposeBoxCustomerData.address1 = customerDataDictionary["address1"] as? String
        exposeBoxCustomerData.address2 = customerDataDictionary["address2"] as? String
        exposeBoxCustomerData.city = customerDataDictionary["city"] as? String
        exposeBoxCustomerData.country = customerDataDictionary["country"] as? String
        exposeBoxCustomerData.state = customerDataDictionary["state"] as? String
        exposeBoxCustomerData.zipCode = customerDataDictionary["zipCode"] as? String
        exposeBoxCustomerData.phone = customerDataDictionary["phone"] as? String
        exposeBoxCustomerData.freeData = customerDataDictionary["freeData"] as? [String: String]
        exposeBox.setCustomerData(customerData: exposeBoxCustomerData,
                                  onSuccess:  {
                                    resultsDict.updateValue(true, forKey: "success")
                                    callback([NSNull(), resultsDict]) },
                                  onFailed: { error in
                                    resultsDict.updateValue(false, forKey: "success")
                                    callback([error, resultsDict])
        })
    }
    
    
    @objc
    func recommendations(_ placementIds: [String], callback: @escaping RCTResponseSenderBlock) -> Void {
        // placementId: dictionary of indentifier of placement
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "recommendations"] as [String: Any]
        exposeBox.recommendations(placementIds: placementIds,
                                  onSuccess:  { response in
                                    let encoder = JSONEncoder()
                                    encoder.outputFormatting = .prettyPrinted
                                    do {
                                        let jsonData = try encoder.encode(response)
                                        if let jsonString = String(data: jsonData, encoding: .utf8) {
                                            print(jsonString)
                                            resultsDict.updateValue(jsonString, forKey: "response")
                                            resultsDict.updateValue(true, forKey: "success")
                                        }
                                    } catch {
                                        print(error.localizedDescription)
                                    }
                                    callback([NSNull(), resultsDict]) },
                                  onFailed: { error in
                                    resultsDict.updateValue(false, forKey: "success")
                                    callback([error, resultsDict])
        })
    }
    
    @objc
    func realImpression(_ batches: [NSDictionary], callback: @escaping RCTResponseSenderBlock) -> Void {
        // batches: dictionary of batches
        // callback: callback to get result of api call

        // create dictionary, if success/fail result - return in RN
        var resultsDict = ["method": "realImpression"] as [String: Any]
        let mappedBatches = batches.map { (batch) in
            ExposeBoxRealImpressionBatch(placementId: batch["placementId"] as! String,
                                         widgetId: (batch["widgetId"] as! NSNumber).intValue,
                                         items: batch["items"] as! [[String : String]]?
        )}
        exposeBox.realImpression(mappedBatches,
                                 onSuccess:  {
                                    resultsDict.updateValue(true, forKey: "success")
                                    callback([NSNull(), resultsDict]) },
                                 onFailed: { error in
                                    resultsDict.updateValue(false, forKey: "success")
                                    callback([error, resultsDict])
        })
    }
    
    @objc
    func resetSession() -> Void {
        exposeBox.resetSession()
    }

    @objc
    func constantsToExport() -> [AnyHashable : Any]! {
        return ["hasGDPR": exposeBox.hasGDPR, "hasCoppaCompliance": exposeBox.hasCoppaCompliance]
    }
    
}
