const {join} = require('path');

exports.config = {
  runner: 'local',
  port: 4723,
  path: '/wd/hub',
  specs: ['./test/enabled/specs/**/*.js'],
  maxInstances: 10,
  capabilities: [
    {
      maxInstances: 1,
      browserName: '',
      appiumVersion: '1.18.1',
      platformName: 'iOS',
      platformVersion: '13.3',
      deviceName: 'iPhone 11',
      app: '/Users/admin/exposeBoxLibrary/ios/exposeBoxLibrary.app',
      automationName: 'XCUITest',
    },
  ],
  logLevel: 'info',
  bail: 0,
  baseUrl: 'http://localhost',
  waitforTimeout: 50000,
  sessionTimeout: 50000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: ['chromedriver'],
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000,
    compilers: [require.resolve('@babel/register')],
  },
};
