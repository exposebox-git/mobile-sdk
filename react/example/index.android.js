/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppAndroid from './App.android';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppAndroid);
