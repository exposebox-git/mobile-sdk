/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Text,
  View,
  Modal,
  NativeModules,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import ExposeBoxSDK from 'react-native-expose-box';

class App extends Component {
  state = {
    isOpen: false,
    result: '',
    error: '',
    recommendation: '',
    callMethod: '',
    isOpenData: false,
  };

  componentDidMount() {
    ExposeBoxSDK.configure('30510', 'com.react.native.com', true, true);
    const constants = ExposeBoxSDK.getConstants();
    this.setState({
      isCoppaComplianceEnable: constants.isCoppaComplianceEnable,
      isGDPREnable: constants.isGDPREnable,
    });
  }

  handleSuccess = (error, data) => {
    if (error) {
      this.setState({error, isOpenData: true});
    }
    if (data) {
      if (data.method === 'recommendations') {
        const parseRecommendation = JSON.parse(data.responseData);
        this.setState({
          result: data,
          isOpenData: true,
          recommendation: data.responseData,
        });
      }
      this.setState({result: data, isOpenData: true});
    }
  };

  toggleDataModal = () => {
    this.setState({isOpenData: false, result: '', callMethod: ''});
  };

  toggleModal = () => {
    this.setState({isOpen: false});
  };

  handleView = () => {
    this.setState({
      callMethod: 'ExposeBoxSDK.view("mainView", this.handleSuccess)',
    });
    ExposeBoxSDK.view('mainView', this.handleSuccess);
  };

  handleSetProducts = () => {
    this.setState({
      callMethod:
        "ExposeBoxSDK.setProducts(['product-8', 'product-7'], this.handleSuccess)",
    });
    ExposeBoxSDK.setProducts(['product-8', 'product-7'], this.handleSuccess);
  };

  handleAddToWishlist = () => {
    this.setState({
      callMethod: 'ExposeBoxSDK.addToWishlist("product-7", this.handleSuccess)',
    });
    ExposeBoxSDK.addToWishlist('product-7', this.handleSuccess);
  };

  handleRemoveFromWishlist = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.removeFromWishlist("product-8", this.handleSuccess)',
    });
    ExposeBoxSDK.removeFromWishlist('product-8', this.handleSuccess);
  };

  handleSetCategories = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.setCategories(["testCat", "testCat"], this.handleSuccess)',
    });
    ExposeBoxSDK.setCategories(['testCat', 'testCat'], this.handleSuccess);
  };

  handleAddToCart = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.addToCart("product-7", 15, 410, this.handleSuccess)',
    });
    ExposeBoxSDK.addToCart('product-7', 15, 410, this.handleSuccess);
  };

  handleRemoveFromCart = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.removeFromCart("product-8", 5, this.handleSuccess)',
    });
    ExposeBoxSDK.removeFromCart('product-8', 5, this.handleSuccess);
  };

  handleClick = () => {
    this.setState({
      callMethod:
        "ExposeBoxSDK.click('mainPagePlacement', 2180, 'product-7', this.handleSuccess)",
    });
    ExposeBoxSDK.click(
      'mainPagePlacement',
      2180,
      'product-7',
      null,
      this.handleSuccess,
    );
    // ExposeBoxSDK.click(
    //   'mainPagePlacement',
    //   2180,
    //   'product-8',
    //   {bsrId: 'ffff'},
    //   this.handleSuccess,
    // );
  };

  handleCustomEvent = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.customEvent("TestCustomEvent", {"Test": "Test"}, this.handleSuccess)',
    });
    ExposeBoxSDK.customEvent(
      'TestCustomEvent',
      {Test: 'Test'},
      this.handleSuccess,
    );
  };

  handleRecommendations = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.recommendations(["mainPagePlacement"], this.handleRecommendation)',
    });
    ExposeBoxSDK.recommendations(['mainPagePlacement'], this.handleSuccess);
  };

  handleSetTags = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.setTags({"test": ["tagTest", "tagTest2"]}, this.handleSuccess)',
    });
    ExposeBoxSDK.setTags({test: ['tagTest', 'tagTest2']}, this.handleSuccess);
  };

  handleConversion = () => {
    this.setState({
      callMethod:
        "ExposeBoxSDK.conversion('3377777', 600.45, [{productId: 'product-8', quantity: 2, unitPrice: 200.02},{productId: 'product-7', quantity: 2, unitPrice: 200.02}], this.handleSuccess)",
    });
    ExposeBoxSDK.conversion(
      '3377777',
      600.45,
      [
        {productId: 'product-8', quantity: 2, unitPrice: 200.02},
        {productId: 'product-7', quantity: 2, unitPrice: 200.02},
      ],
      this.handleSuccess,
    );
  };

  handleSetCustomerData = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.setCustomerData({"email":"test@gmail.com","customerId":"2014", "firstName":"test", "freeData": {"hobby":"fff"}}, this.handleSuccess)',
    });
    ExposeBoxSDK.setCustomerData(
      {
        email: 'test@gmail.com',
        customerId: '2014',
        firstName: 'test',
        freeData: {hobby: 'fff'},
      },
      this.handleSuccess,
    );
  };

  handleRealImpression = () => {
    this.setState({
      callMethod:
        'ExposeBoxSDK.realImpression([{"placementId":"mainPagePlacement", "widgetId":2180,"items":[{"id": "gtest12", "itemType": "product", "catalogId": "default", "category": "leonardo-negev-beer-sheba", "brand": "Leonardo"}, {"id": "gtest12", "itemType": "product", "catalogId": "default", "category": "leonardo-negev-beer-sheba", "brand": "Leonardo"}]}, this.handleSuccess)',
    });
    ExposeBoxSDK.realImpression(
      [
        {
          placementId: 'mainPagePlacement',
          widgetId: 2180,
          items: [
            {
              id: 'gtest12',
              itemType: 'product',
              catalogId: 'default',
              category: 'leonardo-negev-beer-sheba',
              brand: 'Leonardo',
            },
            {
              id: 'gtest12',
              itemType: 'product',
              catalogId: 'default',
              category: 'leonardo-negev-beer-sheba',
              brand: 'Leonardo',
            },
          ],
        },
      ],
      this.handleSuccess,
    );
  };

  handleResetSession = () => {
    ExposeBoxSDK.resetSession();
  };

  renderModalData = () => {
    return (
      <Modal
        animationType="fade"
        transparent
        visible={this.state.isOpenData}
        presentationStyle="overFullScreen">
        <ScrollView
          contentContainerStyle={{
            margin: 20,
            padding: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              margin: 20,
              padding: 20,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
            }}
            testID="popup"
            accessibilityLabel="popup">
            <TouchableOpacity
              style={styles.buttonClose}
              testID="closeModal"
              accessibilityLabel="closeModal"
              onPress={this.toggleDataModal}>
              <Text style={styles.text}>X</Text>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', padding: 5}}>
              <Text>Call method: </Text>
              <Text>{this.state.callMethod}</Text>
            </View>
            {this.state.result.method ? (
              <View style={{flexDirection: 'row', padding: 5}}>
                <Text>Method: </Text>
                <Text testID="method" accessibilityLabel="method">
                  {this.state.result.method}
                </Text>
              </View>
            ) : null}

            {!!this.state.result && (
              <View style={{flexDirection: 'row', padding: 5}}>
                <Text>result: </Text>
                <Text testID="result" accessibilityLabel="result">
                  {this.state.result.success ? 'success' : 'fail'}
                </Text>
              </View>
            )}
            {this.state.result.method === 'recommendations' && (
              <View style={{flexDirection: 'row', padding: 5}}>
                <Text>recommendations: </Text>
                <Text>{this.state.recommendation}</Text>
              </View>
            )}
          </View>
        </ScrollView>
      </Modal>
    );
  };

  renderModalWithoutData = () => {
    return (
      <Modal
        animationType="fade"
        transparent
        visible={this.state.isOpen}
        presentationStyle="overFullScreen">
        <ScrollView
          contentContainerStyle={{
            margin: 20,
            padding: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              margin: 20,
              padding: 20,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
            }}
            testID="popup"
            accessibilityLabel="popup">
            <TouchableOpacity
              style={styles.buttonClose}
              testID="closeModal"
              accessibilityLabel="closeModal"
              onPress={this.toggleModal}>
              <Text style={styles.text}>X</Text>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', padding: 5}}>
              <Text>
                The publisher is trying to send personal data when consent =
                false
              </Text>
            </View>
          </View>
        </ScrollView>
      </Modal>
    );
  };

  render() {
    const {isCoppaComplianceEnable, isGDPREnable} = this.state;
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            {this.renderModalData()}
            {this.renderModalWithoutData()}
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleAddToWishlist}
                testID="addToWishlist"
                accessibilityLabel="addToWishlist">
                <Text style={styles.text}>addToWishlist</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleRemoveFromWishlist}
                testID="removeFromWishlist"
                accessibilityLabel="removeFromWishlist">
                <Text style={styles.text}>removeFromWishlist</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleSetProducts}
                accessibilityLabel="setProductsButton">
                <Text style={styles.text}>setProducts</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleSetTags}
                accessibilityLabel="setTags">
                <Text style={styles.text}>setTags</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleAddToCart}
                accessibilityLabel="addToCart">
                <Text style={styles.text}>addToCart</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleRemoveFromCart}
                testID="removeFromCart"
                accessibilityLabel="removeFromCart">
                <Text style={styles.text}>removeFromCart</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleSetProducts}
                testID="setProductsButton"
                accessibilityLabel="setProductsButton">
                <Text style={styles.text}>setProducts</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleView}
                accessibilityLabel="viewButton">
                <Text style={styles.text}>view</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleSetCategories}
                testID="setCategories"
                accessibilityLabel="setCategories">
                <Text style={styles.text}>setCategories</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleSetCustomerData}
                testID="setCustomerData"
                accessibilityLabel="setCustomerData">
                <Text style={styles.text}>setCustomerData</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleCustomEvent}
                testID="customEvent"
                accessibilityLabel="customEvent">
                <Text style={styles.text}>customEvent</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleConversion}
                accessibilityLabel="conversion">
                <Text style={styles.text}>conversion</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleClick}
                accessibilityLabel="click">
                <Text style={styles.text}>click</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonSmall}
                onPress={this.handleRealImpression}
                testID="realImpression"
                accessibilityLabel="realImpression">
                <Text style={styles.text}>realImpression</Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={styles.button}
              onPress={this.handleRecommendations}
              testID="recommendations"
              accessibilityLabel="recommendations">
              <Text style={styles.text}>recommendations</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleResetSession}
              testID="resetSession"
              accessibilityLabel="resetSession">
              <Text style={styles.text}>resetSession</Text>
            </TouchableOpacity>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  button: {
    backgroundColor: '#00bdff',
    width: 300,
    height: 40,
    borderRadius: 6,
    justifyContent: 'center',
    marginBottom: 10,
    alignSelf: 'center',
  },
  buttonClose: {
    backgroundColor: '#00bdff',
    width: 40,
    height: 30,
    borderRadius: 6,
    justifyContent: 'center',
    marginBottom: 10,
    alignSelf: 'flex-end',
  },
  text: {
    color: '#ffffff',
    alignSelf: 'center',
    fontSize: 15,
    lineHeight: 20,
    letterSpacing: 0,
    fontWeight: '600',
  },
  buttonSmall: {
    backgroundColor: '#00bdff',
    width: 145,
    height: 40,
    borderRadius: 6,
    justifyContent: 'center',
    marginBottom: 10,
    alignSelf: 'center',
  },
  buttonContainer: {
    width: 300,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
});

export default App;
