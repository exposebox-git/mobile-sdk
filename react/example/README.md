"How to" test demo application

You should install react-native-cli: npm install -g react-native-cli (https://reactnative.dev/docs/environment-setup)

Open expose-box folder -> yarn link -> go to Example -> yarn link react-native-expose-box
in package.json for "react-native-expose-box" set path to expose-box folder "file:../expose-box" (it woulbe better put Example and expose-box folders in one place to won't change path)
yarn install or npm install
for ios: cd ios && pod install

Usage:

The APP won't send the data if the GDPR or COOPPA values into configure method are set to false.
If GDPR and COOPPA values are set to true, after click on button you'll see modals with result
If GDPR or COOPPA values are set to false, after click on button you won't see modals with result, because the APP won't send the data

Run your app:
Android: react-native run-android
IOS: react-native run-ios

Automation Testing

Before test:

IOS

1. npm install -g appium (current test works with 1.17.1)
   For test case when coppa and gdpr are enabled we set to wdio.config.js on the line 7 (specs): ['./test/enabled/specs/**/*.js']
   For case when coppa or gdpr are disabled we should set to wdio.config.js on the line 7 (specs): ['./test/disabled/specs/**/*.js']
2. react-native run-ios --configuration Release
3. go to xcode -> open Products folder -> click right button, show in finder -> copy .app -> paste into ios folder
4. in wdio.config set path to .app
5. npm install -g appium-doctor install to see the errors you have to fix before continuing to the next step
6. yarn run appium-doctor
7. appium - put on terminal to start driver
8. launch simulator points in the wdio.config.js
9. npx wdio ./wdio.conf.js or yarn ios.test

Android

1. npm install -g appium (current test works with 1.17.1)
   For test case when coppa and gdpr are enabled we set in wdio.config.js on the line 7 (specs): ['./test/enabled/specs/**/*.js']
   For case when coppa or gdpr are disabled we should set in wdio.config.js on the line 7 (specs): ['./test/disabled/specs/**/*.js']
2. cd android && ./gradlew app:assembleRelease
3. in wdio.android.app.config set path to .apk
4. npm install -g appium-doctor install to see the errors you have to fix before continuing to the next step
5. yarn run appium-doctor
6. Check wdio.android.app.config.js device name
7. appium - to start driver
8. react-native run-android
9. yarn android.test

Useful resources for testing:

1. https://appium.readthedocs.io/en/latest/en/commands/mobile-command/
2. http://appium.io/docs/en/commands/mobile-command/
3. https://webdriver.io/docs/api.html
