function getResult(method) {
  $('~popup').waitForDisplayed(11000, false);
  $('~method').waitForDisplayed(11000, false);
  $('~result').waitForDisplayed(11000, false);
  expect($('~method').getText()).toEqual(method);
  expect($('~result').getText()).toEqual('success');
  $('~closeModal').click();
}

describe('ExposeBox SDK', () => {
  it('should display view button', () => {
    $('~viewButton').waitForDisplayed(11000, false);
    $('~viewButton').click();
    getResult('view');
  });
  it('should display setProducts Button', () => {
    $('~setProductsButton').waitForDisplayed(11000, false);
    $('~setProductsButton').click();
    getResult('setProducts');
  });

  it('should display addToWishlist', () => {
    $('~addToWishlist').waitForDisplayed(11000, false);
    $('~addToWishlist').click();
    getResult('addToWishlist');
  });
  it('should display removeFromWishlist', () => {
    $('~removeFromWishlist').waitForDisplayed(11000, false);
    $('~removeFromWishlist').click();
    getResult('removeFromWishlist');
  });
  it('should display setCategories', () => {
    $('~setCategories').waitForDisplayed(11000, false);
    $('~setCategories').click();
    getResult('setCategories');
  });

  it('should display addToCart', () => {
    $('~addToCart').waitForDisplayed(11000, false);
    $('~addToCart').click();
    getResult('addToCart');
  });
  it('should display removeFromCart', () => {
    $('~removeFromCart').waitForDisplayed(11000, false);
    $('~removeFromCart').click();
    getResult('removeFromCart');
  });
  it('should display click', () => {
    $('~click').waitForDisplayed(11000, false);
    $('~click').click();
    getResult('click');
  });
  it('should display customEvent', () => {
    $('~customEvent').waitForDisplayed(11000, false);
    $('~customEvent').click();
    getResult('customEvent');
  });
  it('should display recommendations', () => {
    $('~recommendations').waitForDisplayed(11000, false);
    $('~recommendations').click();
    getResult('recommendations');
  });
  it('should display setTags', () => {
    $('~setTags').waitForDisplayed(11000, false);
    $('~setTags').click();
    getResult('setTags');
  });
  it('should display conversion', () => {
    $('~conversion').waitForDisplayed(11000, false);
    $('~conversion').click();
    getResult('conversion');
  });
  it('should display setCustomerData', () => {
    $('~setCustomerData').waitForDisplayed(11000, false);
    $('~setCustomerData').click();
    getResult('setCustomerData');
  });
  it('should display realImpression', () => {
    $('~realImpression').waitForDisplayed(11000, false);
    $('~realImpression').click();
    getResult('realImpression');
  });
});
