describe('ExposeBox SDK', () => {
  it('should display view button', () => {
    $('~viewButton').waitForDisplayed(11000, false);
    $('~viewButton').click();
    driver.pause(3000);
  });
  it('should display setProducts Button', () => {
    $('~setProductsButton').waitForDisplayed(11000, false);
    $('~setProductsButton').click();
    driver.pause(3000);
  });

  it('should display addToWishlist', () => {
    $('~addToWishlist').waitForDisplayed(11000, false);
    $('~addToWishlist').click();
    driver.pause(3000);
  });
  it('should display removeFromWishlist', () => {
    $('~removeFromWishlist').waitForDisplayed(11000, false);
    $('~removeFromWishlist').click();
    driver.pause(3000);
  });
  it('should display setCategories', () => {
    $('~setCategories').waitForDisplayed(11000, false);
    $('~setCategories').click();
    driver.pause(3000);
  });

  it('should display addToCart', () => {
    $('~addToCart').waitForDisplayed(11000, false);
    $('~addToCart').click();
    driver.pause(3000);
  });
  it('should display removeFromCart', () => {
    $('~removeFromCart').waitForDisplayed(11000, false);
    $('~removeFromCart').click();
    driver.pause(3000);
  });
  it('should display click', () => {
    $('~click').waitForDisplayed(11000, false);
    $('~click').click();
    driver.pause(3000);
  });
  it('should display customEvent', () => {
    $('~customEvent').waitForDisplayed(11000, false);
    $('~customEvent').click();
    driver.pause(3000);
  });
  it('should display recommendations', () => {
    $('~recommendations').waitForDisplayed(11000, false);
    $('~recommendations').click();
    driver.pause(3000);
  });
  it('should display setTags', () => {
    $('~setTags').waitForDisplayed(11000, false);
    $('~setTags').click();
    driver.pause(3000);
  });
  it('should display conversion', () => {
    $('~conversion').waitForDisplayed(11000, false);
    $('~conversion').click();
    driver.pause(3000);
  });
  it('should display setCustomerData', () => {
    $('~setCustomerData').waitForDisplayed(11000, false);
    $('~setCustomerData').click();
    driver.pause(3000);
  });
  it('should display realImpression', () => {
    $('~realImpression').waitForDisplayed(11000, false);
    $('~realImpression').click();
    driver.pause(3000);
  });
});
