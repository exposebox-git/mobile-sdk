const {join} = require('path');

exports.config = {
  runner: 'local',
  port: 4723,
  path: '/wd/hub',
  specs: ['./test/enabled/specs/**/*.js'],
  maxInstances: 10,
  capabilities: [
    {
      platformName: 'Android',
      maxInstances: 1,
      deviceName: 'Xiaomi Mi A1 ',
      // appiumVersion: '1.17.1',
      automationName: 'UiAutomator2',
      app: join(
        process.cwd(),
        '/android/app/build/outputs/apk/debug/app-debug.apk',
      ),
    },
  ],
  logLevel: 'info',
  bail: 0,
  baseUrl: 'http://localhost',
  waitforTimeout: 50000,
  sessionTimeout: 50000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: ['chromedriver'],
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000,
    compilers: [require.resolve('@babel/register')],
  },
};
